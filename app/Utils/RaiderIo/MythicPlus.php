<?php

namespace App\Utils\RaiderIo;

use App\Models\Character as CharacterModel;

class MythicPlus
{
    // User provided fields
    public string $region;
    public string $characterName;
    public string $realm;
    public int    $characterId;

    // Api fields
    public int $period;
    public int $periodStart;
    public int $periodEnd;

    public function __construct($name, $id, $realm, $region)
    {
        $this->region          = $region;
        $this->realm           = $realm;
        $this->characterName   = $name;
        $this->characterId     = $id;
        $this->getPeriod();
    }

    public function getWeeklyAffixes()
    {
        $query = http_build_query([
            'region' => $this->region,
        ]);
        $url = "https://raider.io/api/v1/mythic-plus/affixes?".$query;
        $client = new Client();
        $response = $client->getFromUrl($url);
        return $response['response'];  
    }

    public function getWeeklyKeystones()
    {
        $query = http_build_query([
            'region' => $this->region,
            'realm'  => $this->realm,
            'name'   => $this->characterName,
            'fields' => 'mythic_plus_recent_runs,mythic_plus_weekly_highest_level_runs,mythic_plus_scores_by_season:current'
        ]);
        $url = "https://raider.io/api/v1/characters/profile?".$query;

        $client   = new Client();
        $response = $client->getFromUrl($url);
        $data     = $response['response'];

        $return = [
            'score' => 0,
            'weekly_keys' => [],
        ];
        if (isset($data->mythic_plus_scores_by_season) && isset($data->mythic_plus_scores_by_season[0])) {
            $return['score'] = $data->mythic_plus_scores_by_season[0]->scores->all;
        }

        if (isset($data->mythic_plus_weekly_highest_level_runs)) {
            $return['weekly_keys'] = $data->mythic_plus_weekly_highest_level_runs;
        }

        if (isset($data->mythic_plus_recent_runs)) {
            $return['weekly_keys'] = array_merge($return['weekly_keys'], $data->mythic_plus_recent_runs);
        }

        return $return;
    }

    public function getPeriod()
    {
        $url = "https://raider.io/api/v1/periods";
        $client = new Client();
        $response = $client->getFromUrl($url);
        $data = $response['response'];
        if (isset($data->periods)) {
            foreach ($data->periods as $p) {
                if (isset($p->region)) {
                    if ($p->region == strtolower($this->region)) {
                        $this->period = $p->current->period;
                        $this->periodStart = strtotime($p->current->start);
                        $this->periodEnd   = strtotime($p->current->end);
                        return;
                    }
                }
            }
        }
    }

    protected function slugify($name): string
    {
        $name = str_replace(' ', '-', $name);
        $name = str_replace("'", '', $name);
        return strtolower($name);
    }
}
