<?php

namespace App\Jobs\Character\Warcraftlogs;

use App\Jobs\Unique;
use App\Models\RaidParse;
use App\Models\GreatVault;
use App\Utils\Warcraftlogs\Parses;
use App\Utils\RaiderIo\MythicPlus;

class UpdateRaidlogs extends Unique
{
    // Job execution handler
    public function handle(): void
    {
        // TODO: make this be based on team information
        $resetRegion = 'us';
        $raiderio = new MythicPlus('', 0, '', $resetRegion);

        $wcl = new Parses(
            $this->model->name,
            $this->model->realm,
            $this->model->region
        );

        $parses = $wcl->getWeeklyBossKills();
        if ($parses != null) {
            // This character has one or more parses
            foreach ($parses as $parse) {
                // Looping through each parse to see if they're in the window of our period
                $parseTime = floor($parse->startTime / 1000);
                if ($parseTime >= $raiderio->periodStart && $parseTime <= $raiderio->periodEnd) {
                    // We've confirmed the parse is from our time period
                    $lookup = RaidParse::where([
                        'character_id' => $this->model->id,
                        'encounter'    => $parse->encounterName,
                        'period'       => $raiderio->period,
                    ]);
                    if ($lookup->count() == 0) {
                        // Getting the current week
                        $kill = new RaidParse();
                        $kill->character_id = $this->model->id;
                        $kill->encounter    = $parse->encounterName;
                        $kill->start_time   = $parse->startTime;
                        $kill->period       = $raiderio->period;
                        $kill->difficulty   = $parse->difficulty;
                    } else {
                        $kill = $lookup->first();
                        if ($kill->difficulty < $parse->difficulty) {
                            $kill->difficulty = $parse->difficulty;
                        }
                    }
                    $kill->save();
                }
            }
        }
    }
}
