@extends('v2.core.main')

@section('headline')
Settings and Preferences
@endsection

@section('content')
<!-- Team Settings Section -->
<section class="mb-4 mb-lg-5">
    <div class="row">

        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-header">
                    <h3 class="card-heading" style="font-size: 1.6rem;">Team Settings</h3>
                </div>
                <div class="card-body px-4 py-4">
                    <form action="{{$team->url()}}/settings" method="POST">
                        @csrf
                        <div class="input-group mb-4">
                            <label class="rk-bg-accent input-group-text" for="team_name">Team Name</label>
                            <input class="form-control" type="text" name="team_name" id="team_name" value="{{$team->name}}">
                        </div>

                        <div class="input-group mb-4">
                            <label class="rk-bg-accent input-group-text" for="team_owner">Team Owner</label>
                            <input class="form-control" type="text" name="owner" id="owner" value="{{$owner}}" disabled>
                        </div>

                        <div class="input-group mb-4">
                            <label class="rk-bg-accent input-group-text" for="public">Public Team:</label>
                            <select class="form-select" name="public[]" id="public">
                                <option value="true" @if ($public) {{"selected"}} @endif>Yes</option>
                                <option value="false" @if (! $public) {{"selected"}} @endif>No</option>
                            </select>
                        </div>

                        <!-- Overview Card Setting -->
                        <div class="row mb-5">
                            <div class="col-md-5 form-label">
                                Featured Overview Callouts
                                <br>
                                <span style="font-size: 11px;">These are the boxes at the top of the Overview page calling out important team stats.</span>
                            </div>
                            <div class="col-md-7">
                                @foreach(['total_members', 'team_ilvl', 'tank_ilvl', 'healer_ilvl', 'weekly_keys'] as $type)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="{{$type}}" name="callouts[]" value="{{$type}}" {{str_contains($settings->firstWhere('key', 'overview_callouts_section'), $type) ? 'checked' : ''}}>
                                        <label class="custom-control-label" for="{{$type}}">{{ucwords(str_replace('_', ' ', $type))}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <!-- Target Keystone Level -->
                        <div class="row mb-5">
                            <div class="col-md-5 form-label">
                                Target Keystone Level
                                <br>
                                <span style="font-size: 11px;">The level of keystone you are targeting for your members to complete weekly.</span>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label class="form-label" for="keystone_target_level">Level:</label>
                                    <input class="form-control" type="number" name="keystone_target_level" value="{{$settings->firstWhere('key', 'keystone_target_level')}}">
                                </div>
                            </div>
                        </div>

                        <div class="input-group justify-content-center">
                            <input type="submit" class="btn btn-primary btn-lg" value="Update Settings">
                        </div>
                    </form>
                </div>
            </div>

            <!-- Roster Card -->
            <div class="card mb-3">
                <div class="card-header">
                    <h3 class="card-heading" style="font-size: 1.6rem;">Roster Settings</h3>
                </div>
                <div class="card-body px-4 py-4">
                    <form action="{{$team->url()}}/settings" method="POST">
                        @csrf

                        @foreach($teamSettings as $slug => $setting)
                        <!-- Roster Column Setting -->
                        <div class="row mb-5">
                            <div class="col-md-5 form-label">
                                {{$setting['name']}}
                                <br>
                                <span style="font-size: 11px;">{{$setting['description']}}</span>
                            </div>
                            <div class="col-md-7">
                                @foreach($setting['options'] as $type => $selected)
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="{{$type}}" name="{{$setting['field']}}" value="{{$type}}" {{$selected}}>
                                        <label class="custom-control-label" for="{{$type}}">{{ucwords(str_replace('_', ' ', $type))}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @endforeach

                        <div class="input-group justify-content-center">
                            <input type="submit" class="btn btn-primary btn-lg" value="Update Settings">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-header">
                    <h3 class="card-heading" style="font-size: 1.6rem;">Team Officers</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <h4>Current Officers</h4>
                            @foreach($team->officers()->get() as $officer)
                            <form class="mx-2 my-1" action="{{$team->url()}}/settings/officers/demote" method="POST">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$officer->id}}">
                                <input type="submit" class="btn btn-danger" value="{{$officer->battletag}}">
                            </form>
                        @endforeach
                    </div>
                    <div class="row mt-3">
                        <h4>Add New Officer</h4>
                        <form action="{{$team->url()}}/settings/officers/promote" method="POST">
                            @csrf
                            <div class="form-group mb-3">
                                <label for="battletag" class="form-label">Battletag:</label>
                                <input type="text" class="form-control" name="battletag" id="battletag">
                            </div>
                            <div class="form-group mb-3">
                                <input type="submit" class="btn btn-primary" value="Add Officer">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection