<section class="text-center">
    <div class="alert show fade alert-dismissable alert-{{session('alert') ?? 'alert-danger'}}" role="alert">
        {{ session('message')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</section>