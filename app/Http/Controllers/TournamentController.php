<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Builder;

class TournamentController extends Controller
{   
    protected $activeTournament = [
        'team_ids' => [9],
        'periods' => [891, 892],
        'required_participants' => 5,
    ];

    protected $pastTournaments = [
        0 => [
            'team_ids' => [9],
            'periods' => [891, 892],
            'required_participants' => 5,           
        ],
    ];

    protected $instanceTimers = [
        
        'Court of Stars' => 30 * 60,
        'Shadowmoon Burial Grounds' => 33 * 60,
        'The Nokhud Offensive' => 40 * 60,
        'The Azure Vault' => 34 * 60,
        'Halls of Valor' => 38 * 60,
        'Ruby Life Pools' => 30 * 60,
        'Temple of the Jade Serpent' => 30 * 60,
        'Algeth\'ar Academy' => 32 * 60,
    ];

    protected $scoreColors = [
        3200 => '#ff8000',
        3040 => '#f9763b',
        2920 => '#f36b5a',
        2800 => '#ea6175',
        2680 => '#e05790',
        2560 => '#d44daa',
        2440 => '#c543c4',
        2320 => '#b23ade',
        2180 => '#9a3fec',
        2060 => '#8252e8',
        1940 => '#6560e4',
        1820 => '#3d6be0',
        1660 => '#2777d6',
        1540 => '#4787c4',
        1420 => '#5698b2',
        1300 => '#5daaa0',
        1180 => '#5fbb8c',
        1060 => '#5ccd76',
        940 => '#53df5e',
        820 => '#40f03d',
        700 => '#3fff26',
        575 => '#89ff6c',
        450 => '#b7ff9f',
        325 => '#ddffd0',
        1 => '#ffffff',
        0 => '#999',
    ];
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (count($this->activeTournament['team_ids']) == 1) {
            // Rat Race Style Tournament
            $team = Team::find($this->activeTournament['team_ids'][0]);
            $periods = $this->activeTournament['periods'];
    
            $characters = $team->characters()->with([
                'attributes' => function (HasMany $query) { $query->whereIn('key', ['bliz_mythic_rating', 'class', 'equipped_ilvl']); },
                'keystones'  => function (BelongsToMany $query) use ($periods) { $query->whereIn('period', $periods)->orderBy('completed_at', 'DESC'); },
            ])->orderBy('name', 'ASC')->get();
            //$participants = $team->characters()->get()->pluck('name')->toArray();
            $participants = $team->characters()->get()->pluck('id')->toArray();
            $participantKeystones = [];
    
            $keystones = [];
            $participantKeystones = [];
            foreach($characters as $character) {
                $keys = $character->keystones;
                foreach($keys as $key) {
                    $keystones[$key->id] = $key;
                }

                $participantKeystones[$character->id] = [
                    'count'   => 0,
                    'highest' => 0,
                ];
            }

            $sortedKeystones = [];
            foreach($keystones as $i => $key) {
                $runners = $key->characters()->whereIn('characters.id', $participants)->get();
                if ($runners->count() < $this->activeTournament['required_participants']) {
                    unset($keystones[$key->id]);
		} else {
		    if ($key->level < 10) { $paddedLevel = "0".$key->level; } else { $paddedLevel = $key->level; }
	            $sortedKeystones[$paddedLevel . "-" . $key->score . "-" . $key->id] = $key;
//                    $sortedKeystones[strtotime($key->completed_at) . "-" . $key->id] = $key;
                    foreach($runners as $runner) {
                        $participantKeystones[$runner->id]['count']++;
                        if ($key->level > $participantKeystones[$runner->id]['highest']){
                            $participantKeystones[$runner->id]['highest'] = $key->level;
                        }
                    }
                }
            }
            $keystones = $sortedKeystones;
            krsort($keystones);
            //dd($keystones);

    
            $instanceTimers = $this->instanceTimers;
            $scoreColors    = $this->scoreColors;
    
            return view(config('rk.version.ui') . '.tournament', compact('team', 'participants', 'participantKeystones', 'characters', 'keystones', 'instanceTimers', 'scoreColors'));
        } elseif (count($this->activeTournament['team_ids']) >= 2) {

        } else {
            return view(config('rk.version.ui') . '.tournament-inactive');
        }
    }
}
