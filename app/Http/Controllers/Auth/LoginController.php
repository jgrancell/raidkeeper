<?php

namespace App\Http\Controllers\Auth;

use App\Providers\RouteServiceProvider;
use App\Http\Controllers\Controller;
use Auth;
use Socialite;
use App\Models\User;
use App\Models\UserActivity;

class LoginController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider(): \Illuminate\Http\RedirectResponse
    {
        $urlPrevious = url()->previous();
        $urlBase     = url()->to('/');

        if (($urlPrevious != $urlBase . '/login') && (substr($urlPrevious, 0, strlen($urlBase)) === $urlBase)) {
            session()->put('url.intended', $urlPrevious);
        }

        return Socialite::driver('battlenet')
            ->scopes(['wow.profile'])
            ->redirect();
    }

    public function handleProviderCallback(): \Illuminate\Http\RedirectResponse
    {
        // Catching exceptions related to not being quick enough with the state
        try {
            $providerUser = Socialite::driver('battlenet')->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }
        $user = $this->createOrGetUser($providerUser);
        Auth::login($user, true);
        return redirect()->intended(RouteServiceProvider::HOME);
    }

    public function createOrGetUser(\SocialiteProviders\Manager\OAuth2\User $providerUser): \App\Models\User
    {
        $lookup = User::where('id', $providerUser->getId());
        if ($lookup->count() == 1) {
            $user = $lookup->first();
        } else {
            $user     = new User;
            $user->id = $providerUser->getId();
        }
        $user->username         = explode("#", $providerUser->getNickname())[0];
        $user->battletag        = $providerUser->getNickname();
        $user->access_token     = $providerUser->token;
        $user->token_expiration = time() - $providerUser->expiresIn;
        $user->save();

        $user->claimCharacters();

        if (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $ip = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        UserActivity::log($user->id, 'login', 'User logged in from '.$ip);
        return $user;
    }
    public function logout(): \Illuminate\Http\RedirectResponse
    {
        Auth::logout();
        return redirect()->to(RouteServiceProvider::HOME);
    }
}
