<table class="table table-hover mb-0" id="rosterTable">
    <thead>
        <tr>
            <th width="7%"></th>
            <th>Character</th>
            @if($filter === null)
            <th class="text-center">Bench Status</th>
            @endif
            <th class="text-center">Item Level</th>
            @if($showColumns['great_vault'])
                <th class="text-center">Weekly Great Vault</th>
            @endif
            @if($showColumns['mythic_score'])
                <th class="text-center">M+ Score</th>
            @endif
            @if($showColumns['tier_special'])
                <th class="text-center">Tier Set Pieces</th>
            @endif
            @can('update', $team)
                <th class="text-center">Tools</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($characters as $character)
            <tr style="vertical-align: middle">
                <!-- Name -->
                <td class="h5 rk-cls-{{str_replace(' ', '', strtolower($character->class))}}">
                    <div style="text-align: center;" class="float-left pull-left">
                        <div>
                            <img style="width: 24px; height: 24px;" src="/static/icons/roles/{{\App\Utils\Formatters\Role::parse($character->pivot->role)}}.svg" alt="{{\App\Utils\Formatters\Role::parse($character->pivot->role)}} Role Icon">
                            @if($character->pivot->trial)
                            <span data-toggle="tooltip" data-placement="top" title="trial member">👶</span>
                            @endif
                        </div>
                        <div>
                            @if(! $character->allow_fetch)
                            <span class="badge rk-bg-demonhunter" style="padding: 0.32em 0.65em; font-size: 11px; width: 100%; font-weight: bold; color: #f5f5f5 !important;">-Disabled-</span>
                            @else
                                @switch(strtolower($character->attributes->where('key', 'faction')->first()->value ?? 'unknown'))
                                    @case('alliance')
                                        <span class="badge rk-bg-shaman" style="padding: 0.32em 0.65em; font-size: 11px; width: 100%; font-weight: bold; color: #f5f5f5 !important;">
                                        @break
                                    @case('horde')
                                        <span class="badge rk-bg-deathknight" style="padding: 0.32em 0.65em; font-size: 11px; width: 100%; font-weight: bold; color: #f5f5f5 !important;">
                                        @break
                                    @default
                                        <span class="badge rk-bg-rogue" style="padding: 0.32em 0.65em; font-size: 11px; width: 100%; font-weight: bold; color: #f5f5f5 !important;">
                                        @break
                                @endswitch
                                {{$character->realm}}</span>
                            @endif
                        </div>
                    </div>
                <td class="h5 rk-cls-{{str_replace(' ', '', strtolower($character->class))}}" style="vertical-align: middle;">
                    <div>
                        <span @if($character->user !== null) data-toggle="tooltip" data-placement="top" title="{{$character->user->battletag}}"@endif>
                            <a href="/members/character/{{$character->id}}?team={{$team->id}}" style="color: inherit;">{{$character->name}}</a>
                        </span>
                    </div>
                </td>
                <!-- Bench Status -->
                @if($filter === null)
                    <td class="text-center">
                        {{$character->pivot->benched ? 'Benched' : 'Active'}}
                    </td>
                @endif
                <!-- Item Level -->
                <td class="text-center">{!! \App\Utils\Formatters\ItemLevel::color($character->attributes->where('key', 'equipped_ilvl')->pluck('value')->avg()) !!}</td>
                <!-- Weekly GV -->
                @if($showColumns['great_vault'])
                    <td class="text-center" style="font-family: monospace;">{!! \App\Utils\Formatters\GreatVault::parse($character->great_vault->first()) !!}</td>
                @endif
                <!-- M+ Score -->
                @if($showColumns['mythic_score'])
                    <td class="text-center">{!! \App\Utils\Formatters\MythicRating::parse($character->attributes->where('key', 'bliz_mythic_rating')->first()->value ?? 0) !!}</td>
                @endif
                <!-- M+ Score -->
                @if($showColumns['tier_special'])
                    <td class="text-center">
                        <div class="d-flex justify-content-center px-5">
                            @foreach([2, 4] as $minimums)
                                <div style="margin-right: 10px;">
                                    @if($character->getTierSpecialCount() >= $minimums)
                                    <span class="rk-cls-monk">✓</span>
                                    @else
                                    <span class="rk-cls-deathknight">X</span>
                                    @endif
                                    <span>{{$minimums}} Piece</span>
                                </div>
                            @endforeach
                    </td>
                @endif
                <!-- Officer Tools -->
                @can('update', $team)
                    <td class="text-center">
                        <a href="#" data-bs-toggle="modal" data-bs-target="#editRaider{{$character->id}}">
                            <svg class="svg-icon svg-icon-md me-3">
                                <use xlink:href="/v2/images/vendor/orion-svg-sprite.svg#more-1"> </use>
                            </svg>
                        </a>
                        <!-- Modal-->
                        <div class="modal fade text-start" style="margin-top: 20px;" id="editRaider{{$character->id}}" tabindex="-1" role="dialog" aria-labelledby="editRaiderLabel{{$character->id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header border-0">
                                        <h3 class="h5 modal-title" id="editRaiderLabel{{$character->id}}">Edit {{$character->name}}</h3>
                                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{$team->url()}}/roster" method="POST" class="mx-3">
                                            @csrf
                                            <p>Toggle the icons below to change {{$character->name}}'s position on your roster.</p>
                                            <input type="hidden" name="character_id" value="{{$character->id}}">
                                            <input type="hidden" name="action" value="edit">
                                            <!-- Modal - Roster Role -->
                                            <div class="form-group mb-4 mt-4">
                                                <label class="form-label" for="role">The character's role on the team</label>
                                                <select class="form-select" type="select" name="role" id="role">
                                                    @foreach($character->getClassRoles() as $name => $id)
                                                        <option value="{{$id}}" @if($id == $character->pivot->role){{'selected'}}@endif>{{ucwords($name)}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <!-- Modal - Bench Status -->
                                            <div class="form-group mb-4">
                                                <label class="form-label" for="role">Is the character benched or active?</label>
                                                <select class="form-select" type="select" name="status" id="status">
                                                    <option value="active" @if($character->pivot->benched == 0){{'selected'}}@endif>Active</option>
                                                    <option value="bench" @if($character->pivot->benched == 1){{'selected'}}@endif>Benched</option>
                                                </select>
                                            </div>

                                            <!-- Modal - Membership Status -->
                                            <div class="form-group mb-4">
                                                <label class="form-label" for="role">Is the character a full member or trial?</label>
                                                <select class="form-select" type="select" name="trial" id="trial">
                                                    <option value="full" @if($character->pivot->trial == 0){{'selected'}}@endif>Full Member</option>
                                                    <option value="trial" @if($character->pivot->trial == 1){{'selected'}}@endif>Trial Member</option>
                                                </select>
                                            </div>

                                            <div class="form-group text-center">
                                                <input type="submit" class="btn btn-primary" value="Update Character">
                                            </div>
                                        </form>
                                    </div>

                                    <!-- Delete Section -->
                                    <div class="modal-header border-0">
                                        <h3 class="h5 modal-title" id="editRaiderLabel{{$character->id}}">Refresh {{$character->name}}</h3>
                                    </div>
                                    <div class="modal-body text-center" style="padding-top: 0px; padding-bottom: 0px;">
                                        <form action="{{$team->url()}}/roster/refresh/{{$character->id}}" method="POST" class="mx-3">
                                            @csrf
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary" value="Refresh">
                                            </div>
                                        </form>
                                    </div>

                                    <!-- Delete Section -->
                                    <div class="modal-header border-0">
                                        <h3 class="h5 modal-title" id="editRaiderLabel{{$character->id}}">Delete {{$character->name}}</h3>
                                    </div>
                                    <div class="modal-body text-center" style="padding-top: 0px;">
                                        <form action="{{$team->url()}}/roster" method="POST">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="character_id" value="{{$character->id}}">
                                            <input type="submit" class="btn btn-danger mx-2" value="Remove From Team">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>