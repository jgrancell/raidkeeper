<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_attributes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('team_id');
            $table->string('key');
            $table->string('value');
            $table->timestamps();

            $table->unique(['team_id', 'key']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_attributes');
    }
}
