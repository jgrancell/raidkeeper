<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Raidkeeper</title>

    <base href="/">
    <link rel="icon" href="/branding/favicons/blue.png">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="https://cdn.lineicons.com/3.0/lineicons.css" rel="stylesheet">
    <link rel="stylesheet" href="/v3/css/app.css">
    <link rel="stylesheet" href="/v3/css/theme-blue.css">
  </head>
  <body>
    @include('v3._components.header')
    <div class="row h-100 rk-main px-0">
      @include('v3._components.sidebar')
      <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 py-4">
        <div class="row">@yield('content')</div>
        <div class="row mt-3">@include('v3._components.footer')</div>
      </main>
 
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script src="v3/js/app.js"></script>
  </body>
</html>