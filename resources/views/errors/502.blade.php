@extends('errors.base')

@section('code', __('502 Bad Gateway'))
@section('message', __('It may have been us. It may have been you. It was probably Blizzard. We\'ve been notified.'))