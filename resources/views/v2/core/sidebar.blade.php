<div class="sidebar py-2" id="sidebar">
    @if(\Auth::check())
        @include('v2.core.sidebar.user')
    @endif
    @if(Request::routeIs('team.*') && Request::route('team'))
        @include('v2.core.sidebar.team')
    @endif
  </div>