@extends('errors.base')

@section('code', __('403 Permission Denied'))

@if(\Auth::check())
  @section('message', __('You do not have permission to access this page.'))
@else
  @section('message', __('You must be logged in to access this page.'))
@endif