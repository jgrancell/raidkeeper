<?php

namespace App\Utils\Battlenet;

use Illuminate\Support\Facades\Cache;

class Token
{
    /**
     * The Oauth2 Client Id for Battlenet
     * @var string
     */
    protected string $clientID;

    /**
     * The Oauth2 Client Secret for Battlenet
     * @var string
     */
    protected string $clientSecret;

    /**
     * The API region we want to get an access token for
     * @var string
     */
    protected string $region;

    /**
     * The access token provided by Battlenet after initial contact
     * @var string
     */
    protected string $accessToken;

    /**
     * The expiration time for the currently stored access token
     * @var string
     */
    protected string $tokenExpiration;

    public function __construct($region)
    {
        $this->clientID     = config('rk.battlenet.id');
        $this->clientSecret = config('rk.battlenet.secret');
        $this->region       = $region;
        $this->refresh();
    }

    protected function refresh(): void
    {
        $cache = Cache::get($this->region.'_token_expiration');

        if ($cache == null || $cache <= (time() - 60)) {
            $this->handshake();
        }
        $this->tokenExpiration = Cache::get($this->region.'_token_expiration');
        $this->accessToken     = Cache::get($this->region.'_token');
    }

    protected function handshake(): void
    {
        $url = 'https://'.$this->region.'.battle.net/oauth/token';
        $headers = [
            'User-Agent: RaidKeeper <github.com/jgrancell/raidkeeper>',
        ];

        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL            => $url,
                CURLOPT_HTTPHEADER     => $headers,
                CURLOPT_POST           => 1,
                CURLOPT_POSTFIELDS     => ['grant_type'=>'client_credentials'],
                CURLOPT_USERPWD        => $this->clientID . ":" . $this->clientSecret,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_SSL_VERIFYPEER => 0,
            ),
        );

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        
        if ($status == 200) {
            $data = json_decode($response);
            $token = $data->access_token;
            $expiration = time() + $data->expires_in;
            Cache::put($this->region . '_token_expiration', $expiration, now()->addHours(23));
            Cache::put($this->region . '_token', $token, now()->addHours(23));
        }
    }

    public function getToken(): string
    {
        return $this->accessToken;
    }
}
