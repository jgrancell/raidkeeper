@extends('v2.core.main')

@section('headline')
Create A Team
@endsection

@section('content')
<!-- Welcome Section -->
<section class="mb-4 mb-lg-5">
    <div class="row">
        <div class="col-lg-6 mb-4 mb-lg-0">
            <h2 class="section-heading section-heading-ms mb-3 mb-lg-3">What Are Teams?</h2>
            <div class="card h-100">
                <div class="card-body px-4 py-4">
                    <p>Teams are a group of characters that you are managing, looking to complete content in WoW. A RaidKeeper team can be configured to support:</p>
                    <ul>
                        <li>Raiding</li>
                        <li>Mythic+ Push Groups</li>
                        <li>Coordinated PvP Groups (Arenas, RBGs, etc)</li>
                    </ul>
                    <p>Raidkeeper also supports managing other groups, such as Mythic Plus and PvP groups. Most features, however, are aimed at raid progression.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4 mb-lg-0">
            <h2 class="section-heading section-heading-ms mb-3 mb-lg-3">Create Your Team!</h2>
            <div class="card h-100">
                <div class="card-body px-4 py-4">
                    <form action="/teams/create" method="POST">
                        @csrf
                        <input type="hidden" name="action" value="create">
                        <div class="input-group mb-2">
                            <p>Only a few pieces of basic configuration are required to set up your team.</p>
                            <label class="input-group-text" for="region">Region:</label>
                            <select class="form-select" name="region" id="region" required>
                                <option value="" disabled selected></option>
                                <option value="us">US</option>
                                <option value="eu">EU</option>
                                <option value="apac">APAC</option>
                            </select>
                        </div>
                        <div class="input-group mb-1">
                            <label class="input-group-text" for="realm">Server:</label>
                            <select class="form-select" name="realm" id="realm" required>
                                <option id="default" disabled selected>--Select Region First--</option>
                            </select>
                        </div>
                        <div class="form-text rk-accent-10 mb-3">You can add characters from other servers, as long as they're in your region.</div>

                        <div class="input-group mb-1">
                            <label class="input-group-text" for="name">Team Name:</label>
                            <input class="form-control" type="text" name="name" id="name" placeholder="Team Name" required>
                        </div>
                        <div class="form-text rk-accent-10 mb-3">Team names need to be unique to a given region.</div>

                        <div class="input-group mb-1">
                            <label class="input-group-text" for="short_name">Short Name:</label>
                            <input class="form-control" type="text" name="short_name" id="short_name" placeholder="A 3-4 letter short name for your team." required>
                        </div>
                        <div class="form-text rk-accent-10 mb-3">We use this instead of the full name on some small form-factors.</div>

                        <div class="input-group mb-1">
                            <label class="input-group-text" for="content">Primary Content:</label>
                            <select class="form-select" name="content" id="content" required>
                                <option disabled selected></option>
                                <option disabled>-- Raiding --</option>
                                <option value="mythic-raiding">Mythic Raiding</option>
                                <option value="heroic-raiding">Heroic Raiding</option>
                                <option value="normal-raiding">Normal Raiding</option>
                                <option disabled>-- Mythic Plus --</option>
                                <option value="mythic+-pushing">Mythic+ Pushing</option>
                                <option value="casual-mythic+">Casual Mythic+</option>
                                <option disabled>-- PvP Content --</option>
                                <option value="rated-arenas">Rated Arenas</option>
                                <option value="rated-battlegrounds">Rated Battlegrounds</option>
                            </select>
                        </div>
                        <div class="form-text rk-accent-10 mb-3">This is the primary content your team runs. This makes you easier to search for.</div>

                        <div class="input-group text-right align-items-right">
                            <input type="submit" class="btn btn-primary text-right" value="Create Team">
                        </div>
                        <div class="form-group"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection