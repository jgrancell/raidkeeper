<?php

namespace App\Routes\Teams;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Teams\IndexController;

class GeneralRoutes
{
    public static function load()
    {
        /**
         * These are general non-specific team routes. These include:
         *   - Team creation
         *   - Team searching
         *   - Region listing and searching
         */

        Route::get('/teams',        [IndexController::class, 'index']);
        Route::get('/teams/create', [IndexController::class, 'form']);

        // We require the user log in before they can POST create a team 
        Route::middleware('auth')->group(function () {
            Route::post('/teams/create', [IndexController::class, 'create']);
        });
    }
}
