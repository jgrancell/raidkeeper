<?php

return [
    'gitlab' => [
        'project_id' => env('GITLAB_PROJECT_ID', 26497666),
        'support_submission_token' => env('GITLAB_SUPPORT_SUBMISSION_TOKEN'),
    ],

    'battlenet' => [
        'id'     => env('BATTLENET_CLIENT_ID'),
        'secret' => env('BATTLENET_CLIENT_SECRET'),
    ],

    'max_level' => env('RK_MAX_LEVEL', 60),

    'warcraftlogs' => [ 
        'api_key'   => env('WARCRAFTLOGS_API_KEY'),
        'raid_zone' => env('WARCRAFTLOGS_RAID_ZONE', 27)
    ],

    'ilvls' => [
        'mythic'      => env('RK_ILVL_MYTHIC', 441),
        'heroic'      => env('RK_ILVL_HEROIC', 428),
        'normal'      => env('RK_ILVL_NORMAL', 415),
        'raid-finder' => env('RK_ILVL_RAIDFINDER', 402),
    ],

    'mplus_thresholds' => [
        'legendary' => [
            'minimum' => env('RK_MPLUS_THRESH_LEGENDARY_MIN', 2000),
            'ilvl'    => env('RK_MPLUS_THRESH_LEGENDARY_ILVL', 298),
        ],
        'epic'      => [
            'minimum' => env('RK_MPLUS_THRESH_EPIC_MIN', 1700),
            'ilvl'    => env('RK_MPLUS_THRESH_EPIC_ILVL', 294),
        ],
        'rare'      => [
            'minimum' => env('RK_MPLUS_THRESH_RARE_MIN', 1400),
            'ilvl'    => env('RK_MPLUS_THRESH_RARE_ILVL', 291),
        ],
        'uncommon'  => [
            'minimum' => env('RK_MPLUS_THRESH_UNCOMMON_MIN', 1000),
            'ilvl'    => env('RK_MPLUS_THRESH_UNCOMMON_ILVL', 285),
        ],
    ],

    'great_vault_thresholds' =>
    [
        'mplus' => [
            'first'  => 1,
            'second' => 4,
            'third'  => 8,
        ],
        'raid'  => [
            'first'  => 3,
            'second' => 5,
            'third'  => 7,
        ],
    ],

    'version' => [
        'application' => 'v1.6.0',
        'ui'          => env('RK_UI_VERSION', 'v2'),
    ]
];
