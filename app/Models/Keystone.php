<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keystone extends Model
{
    use HasFactory;

    public function characters()
    {
        return $this->belongsToMany(Character::class, 'character_keystone', 'keystone_id', 'character_id');
    }

    public function runners()
    {
        $related = Keystone::where([
            'completed_at' => $this->completed_at,
            'dungeon'      => $this->dungeon,
            'level'        => $this->level,
        ])->with('characters')->get();
        return $related->pluck('character')->pluck('name')->toArray();
    }
}
