<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Battlenet
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Character[] $characters
 * @property-read int|null $characters_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Battlenet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Battlenet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Battlenet query()
 */
	class Battlenet extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Character
 *
 * @property int $id
 * @property string $name
 * @property string $region
 * @property string $realm
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $allow_fetch
 * @property string|null $class
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CharacterAttendance[] $attendance
 * @property-read int|null $attendance_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CharacterAttribute[] $attributes
 * @property-read int|null $attributes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CharacterEquipment[] $equipment
 * @property-read int|null $equipment_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GreatVault[] $great_vault
 * @property-read int|null $great_vault_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Keystone[] $keystones
 * @property-read int|null $keystones_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RaidParse[] $parses
 * @property-read int|null $parses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Team[] $teams
 * @property-read int|null $teams_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Character newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Character newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Character query()
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereAllowFetch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereRealm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Character whereUserId($value)
 */
	class Character extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CharacterAttendance
 *
 * @property int $id
 * @property int $character_id
 * @property int $team_attendance_id
 * @property int $team_id
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance query()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance whereTeamAttendanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttendance whereUpdatedAt($value)
 */
	class CharacterAttendance extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CharacterAttribute
 *
 * @property int $id
 * @property int $character_id
 * @property string $key
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterAttribute whereValue($value)
 */
	class CharacterAttribute extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CharacterEquipment
 *
 * @property int $id
 * @property int $character_id
 * @property string $slot
 * @property int $item_id
 * @property string $name
 * @property int $level
 * @property int $socket
 * @property int $enchantment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $borrowed_special
 * @property string|null $bonuses
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment query()
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereBonuses($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereBorrowedSpecial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereEnchantment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereSlot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereSocket($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CharacterEquipment whereUpdatedAt($value)
 */
	class CharacterEquipment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\GreatVault
 *
 * @property int $id
 * @property int $character_id
 * @property int $period
 * @property string|null $raid_1
 * @property string|null $raid_2
 * @property string|null $raid_3
 * @property int|null $key_1
 * @property int|null $key_2
 * @property int|null $key_3
 * @property string|null $pvp_1
 * @property string|null $pvp_2
 * @property string|null $pvp_3
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault query()
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereKey1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereKey2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereKey3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault wherePvp1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault wherePvp2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault wherePvp3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereRaid1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereRaid2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereRaid3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GreatVault whereUpdatedAt($value)
 */
	class GreatVault extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Keystone
 *
 * @property int $id
 * @property int $character_id
 * @property string $dungeon
 * @property int $level
 * @property int $period
 * @property string $completed_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $raiderio_url
 * @property-read \App\Models\Character $character
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone query()
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereDungeon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereRaiderioUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Keystone whereUpdatedAt($value)
 */
	class Keystone extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RaidParse
 *
 * @property int $id
 * @property int $character_id
 * @property string $encounter
 * @property int $start_time
 * @property string $period
 * @property int $difficulty
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse query()
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse whereDifficulty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse whereEncounter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaidParse whereUpdatedAt($value)
 */
	class RaidParse extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Team
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property string $region
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TeamAttribute[] $attributes
 * @property-read int|null $attributes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Character[] $characters
 * @property-read int|null $characters_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $officers
 * @property-read int|null $officers_count
 * @method static \Illuminate\Database\Eloquent\Builder|Team newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team query()
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUserId($value)
 */
	class Team extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamAttendance
 *
 * @property int $id
 * @property int $team_id
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttendance whereUpdatedAt($value)
 */
	class TeamAttendance extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamAttribute
 *
 * @property int $id
 * @property int $team_id
 * @property string $key
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAttribute whereValue($value)
 */
	class TeamAttribute extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamOfficer
 *
 * @property int $id
 * @property int $team_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamOfficer whereUserId($value)
 */
	class TeamOfficer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $username
 * @property string $battletag
 * @property string $access_token
 * @property int $token_expiration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Character[] $characters
 * @property-read int|null $characters_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TeamOfficer[] $officerships
 * @property-read int|null $officerships_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Team[] $owned
 * @property-read int|null $owned_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserSetting[] $settings
 * @property-read int|null $settings_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereBattletag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTokenExpiration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUsername($value)
 */
	class User extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserActivity
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserActivity whereUserId($value)
 */
	class UserActivity extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\UserSetting
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserSetting whereValue($value)
 */
	class UserSetting extends \Eloquent {}
}

