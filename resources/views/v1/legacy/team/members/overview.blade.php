@extends('layouts.main')

@section('content')
<div class="row mx-2">
    <div class="col-12 subsection">
        <div class="card">
            <div class="card-header text-center">
                <h2 class="text-center">{{$team->name}}</h2>
                <a class="subnav-text" href="/teams/{{$team->id}}/overview">Overview</a>
                <a class="subnav-text" href="/teams/{{$team->id}}/members/attendance">Your Attendance</a>
            </div>
        </div>
    </div>

    <!-- Your Roster Character -->
    <div class="col-3 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Your Roster Spot</h3>
                    </div>
                    <div class="card-body text-center">
                        <img src="/img/icons/{{$slot->roleToText()}}.svg" alt="{{$slot->roleToText()}}"></img>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Weekly Checklist -->
    <div class="col-3 subsection">
        <div class="row mx-0 mt-4 mb-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-center">Weekly Raider Checklist</h3>
                    </div>
                    <div class="card-body text-center">
                        <div class="row text-center">
                            <div class="col-3"></div>
                            <div class="col-3"></div>
                            <div class="col-3"></div>
                            <div class="col-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
