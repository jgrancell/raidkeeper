<h2 class="sidebar-heading">Team Tools</h2>
<ul class="list-unstyled">
    <li class="sidebar-list-item">
        <a class="sidebar-link @if(Request::routeIs('team.dashboard')){{'active'}}@endif" href="{{Request::route('team')->url()}}">
            <svg class="svg-icon svg-icon-md me-3">
                <use xlink:href="/v2/images/vendor/orion-svg-sprite.svg#real-estate-1"> </use>
            </svg>
            <span class="sidebar-link-title">Overview</span>
        </a>
    </li>
    @can('view', Request::route('team'))
        <li class="sidebar-list-item">
            <a class="sidebar-link @if(Request::routeIs('team.roster')){{'active'}}@endif" href="{{Request::route('team')->url()."/roster/active"}}"> 
                <svg class="svg-icon svg-icon-md me-3">
                    <use xlink:href="/v2/images/vendor/orion-svg-sprite.svg#man-1"> </use>
                </svg>
                <span class="sidebar-link-title">Roster</span>
            </a>
        </li>
    @endcan
</ul>

@if(\Auth::check() && \Auth::user()->isOfficer(Request::route('team')))
    <h2 class="sidebar-heading">Team Lead Tools</h2>
    <ul class="list-unstyled">
<!--
        <li class="sidebar-list-item">
            <a class="sidebar-link @if(Request::routeIs('team.metrics.*')){{'active'}}@endif" href="#" data-bs-target="#metricsDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
                <svg class="svg-icon svg-icon-md me-3">
                    <use xlink:href="/v2/images/vendor/orion-svg-sprite.svg#chart-1"> </use>
                </svg>
                <span class="sidebar-link-title">Metrics</span>
            </a>
            <ul class="sidebar-menu list-unstyled collapse " id="metricsDropdown">
            <li class="sidebar-list-item"><a class="sidebar-link text-muted @if(Request::routeIs('team.metrics.keystones')){{'active'}}@endif" href="{{Request::route('team')->url()}}/metrics/keystones">Team Keystones</a></li>
            </ul>
        </li>-->

        <li class="sidebar-list-item">
            <a class="sidebar-link @if(Request::routeIs('team.settings')){{'active'}}@endif" href="{{Request::route('team')->url()}}/settings">
                <svg class="svg-icon svg-icon-md me-3">
                    <use xlink:href="/v2/images/vendor/orion-svg-sprite.svg#settings-2"> </use>
                </svg>
                <span class="sidebar-link-title">Settings</span>
            </a>
        </li>
    </ul>
@endif