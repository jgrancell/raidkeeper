<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CacheTeamRosters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rk:cache:rosters';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Builds the RK Cache for all Team rosters.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (\App\Models\Team::all() as $team) {
            \App\Jobs\Team\CacheRoster::dispatch($team);
        }
    }
}
