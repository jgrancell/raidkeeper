<?php

namespace App\Jobs\Character\Battlenet;

use App\Jobs\Unique;
use Raidkeeper\Api\Battlenet\Client;
use Raidkeeper\Api\Battlenet\ApiResponse;

class UpdateProfile extends Unique
{
    // Job execution handler
    public function handle(): void
    {
        $client    = new Client($this->model->region, config('rk.battlenet.id'), config('rk.battlenet.secret'));
        $character = $client->loadCharacter($this->model->name, $this->model->realm);
        $data      = $character->getProfile();
        if ($data instanceof ApiResponse) {
            if ($this->model->id == $data->id) {
                $this->model->name  = $data->name;
                $this->model->realm = $data->realm;

                $this->model->setAttr('gender', $data->gender);
                $this->model->setAttr('faction', $data->faction);
                $this->model->setAttr('race', $data->race);
                $this->model->setAttr('class', $data->character_class);
                $this->model->setAttr('spec', $data->active_spec);
                $this->model->setAttr('level', $data->level);
                $this->model->setAttr('experience', $data->experience);
                $this->model->setAttr('equipped_ilvl', $data->equipped_item_level);
                $this->model->setAttr('bag_ilvl', $data->average_item_level);

                if (! $data->guild instanceof \Error) {
                    $this->model->setAttr('guild', $data->guild);
                }
        
                if (! $data->covenant_progress instanceof \Error) {
                    $this->model->setAttr('covenant', $data->getLocaleData($data->covenant_progress->chosen_covenant));
                    $this->model->setAttr('renown', $data->covenant_progress->renown_level);
                }

                $this->model->save();
            } else {
                // TODO: Setup deregistration for a character here
                // The only way we really get here is if 
                //     1. the person added to the roster gets a name change or transfers off the server
                //     2. a new character is created with the same exact name on the old character's server
                $this->model->allow_fetch = false;
                $this->model->save();
                throw new \Exception('Character '.$this->model->name.'-'.$this->model->realm.' ID does not match.', 500);
            }
        } else {
            if ($data->getCode() == '404') {
                $this->model->allow_fetch = false;
                $this->model->save();
                echo "Character not found in Battlenet API. Disabling from future updates.";
            } else {
                throw new \Exception(
                    'Unable to load character '.$this->model->name.'-'.$this->model->realm.'from Battlenet API. '.$data->getCode().': '.$data->getMessage(), 
                    $data->getCode()
                );
            }
        }
    }
}
