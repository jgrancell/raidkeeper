<div class="d-flex align-items-stretch">
    @include('v2.core.sidebar')
    <div class="page-holder bg-content">
        <div class="container-fluid px-lg-4 px-xl-5">
            <div class="page-header">
                <h1 class="page-heading">@yield('headline')</h1>
            </div>
            @if(session()->has('alert'))
                @include('v2.core.alert')
            @endif
            <section>
                @yield('content')
            </section>
        </div>
        @include('v2.core.footer')
    </div>
</div>