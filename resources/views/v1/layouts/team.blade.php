@extends('v1.layouts.main')

@section('content')
<div class="row mx-2">
    <div class="col-12 subsection">
        <div class="card">
            <div class="card-header text-center">
                <h2 class="text-center">{{$team->name}}</h2>
                <a class="subnav-text first-menu-option" href="/teams/{{$team->id}}">Overview</a>
                <a class="subnav-text" href="/teams/{{$team->id}}/roster">Roster</a>
                <a class="subnav-text" href="/teams/{{$team->id}}/attendance">Attendance</a>
            </div>
        </div>
    </div>
</div>
@yield('subcontent')
@endsection
