<?php

namespace App\Jobs\Team;

use App\Models\Team;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CacheRoster implements ShouldQueue, ShouldBeUnique
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The character instance
     *
     * @var \App\Models\Team
     */
    protected $team;

    /**
     * The filter type we're placing on the benched roster constraint
     *
     * @var mixed
     */
    protected $filter;

    /**
     * The number of seconds after which the job's unique lock will be released
     */
    public $uniqueFor = 300;

    /**
     * The number of after which a job will be terminated.
     */
    public $timeout = 180;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->team->cacheRoster();
    }

    /**
     * The unique model ID of the job
     */
    public function uniqueId()
    {
        return $this->team->id;
    }
}
