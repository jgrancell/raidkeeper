@extends('layouts.main')

@section('content')
    <div class="row mx-2">
        <div class="col-md-6 col-sm-12 subsection">
            <!-- Discord Card -->
            <div class="row mx-1 mt-2 mb-4">
                <div class="col-12">
                    <!-- Lets Make A Team Card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Lets Create A Team!</h3>
                        </div>
                        <div class="body px-2">
                            <p>
                                Teams are the core component of RaidKeeper.
                            </p>
                            <p>
                                A Team is a group of players that are looking to accomplish
                                a raid, mythic+, PvP, or other (possibly outlandish!) goal in
                                World of Warcraft.
                            </p>
                            <p>
                                Creating a team gives you access to a ton of current and planned
                                features, including:
                            </p>
                            <ul>
                                <li>Accepting team applications.</li>
                                <li>Attendance tracking and callout-tracking.</li>
                                <li>Quick access to your team member logs and IO links.</li>
                            </ul>
                            <p>
                                If you don't want to see this welcome text anymore, just finish
                                your account setup and then click the pretty "Hide" button.
                            </p>
                        </div>
                    </div>
                    <!-- Costs -->
                    <div class="card mt-4">
                        <div class="card-header">
                            <h3 class="text-center">How Much Does This Cost?</h3>
                        </div>
                        <div class="body px-2">
                            <p>
                                It's all free for now! Some future features may come as part
                                of Patreon or paid membership access levels, but the core features
                                are free forever.
                            </p>
                            <p>
                                Some of my ideas for paid future features include:
                            </p>
                            <ul>
                                <li>Insight into the content your members have done this week.</li>
                                <li>Warnings for any missing raid buffs.</li>
                                <li>Breakdowns on critial raid healing/mitigation cooldowns.</li>
                                <li>Scheduling tools for your events.</li>
                                <li>Promoted and/or custom Team recruitment pages.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 subsection">
            <!-- Create A Team Form -->
            <div class="row mt-2 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center"> It's Time! Lets Make It!</h3>
                        </div>
                        <form action="/teams/new" method="POST">
                            @csrf
                            <div class="card-body">
                                <!-- Team Name -->
                                <div class="form-group">
                                    <label for="name">Team Name:</label>
                                    <input class="form-control" type="text" name="name" required>
                                    <div class="form-text">This must be unique when combined with the server you provide below. Sorry.</div>
                                </div>

                                <!-- Server -->
                                <div class="form-group mt-3">
                                    <label for="realm">Server:</label>
                                    <input class="form-control" type="text" name="realm" required>
                                    <div class="form-text">What server/realm are you based out of? Examples: Illidan, Sargeras, Your Mom.</div>
                                </div>

                                <!-- Team Faction -->
                                <div class="form-group mt-3">
                                    <label for="faction">Faction:</label>
                                    <input type="radio" name="faction" id="horde" value="horde" checked="checked">
                                    <label class="fac-horde" style="display: inline; font-size: 1.25rem;" for="horde">Horde</label>
                                    <input type="radio" name="faction" id="alliance" value="alliance">
                                    <label class="fac-alliance" style="display: inline; font-size: 1.25rem;" for="alliance">Alliance</label>
                                    <div class="form-text">Please be alliance. They really need the love, you know...</div>
                                </div>

                                <!-- Team Purpose -->
                                <div class="form-group mt-3">
                                    <label for="purpose">Your Team's Purpose:</label>
                                    <select class="form-control" name="purpose" required>
                                        <option value="Raiding">Raiding</option>
                                        <option value="Mythic+">Organized Mythic+</option>
                                        <option value="Rated PvP">Rated PvP - Battlegrounds</option>
                                        <option value="Rated PvP">Rated PvP - Arenas</option>
                                    </select>
                                    <div class="form-text">If you're looking for a category that doesn't exist already reach out.</div>
                                </div>

                                <!-- Your Goal -->
                                <div class="form-group mt-3">
                                    <label for="goal">What's Your Goal:</label>
                                    <select class="form-control" name="goal">
                                        <option value="" disabled>---Raiding---</option>
                                        <option value="Cutting Edge">Cutting Edge</option>
                                        <option value="Ahead of the Curve">Ahead of the Curve</option>
                                        <option value="Tier Completion">General Tier Completion</option>
                                        <option value="" disabled>---Mythic+---</option>
                                        <option value="Keystone Conqueror">Keystone Conqueror</option>
                                        <option value="Keystone Master">Keystone Master</option>
                                        <option value="Mythic Dungeon Invitational / Cup">Mythic Dungeon Invitational / Cup</option>
                                        <option value="" disabled>---Rated RBGs---</option>
                                        <option value="1600+ Rating" disabled>1600+ Rating</option>
                                        <option value="1800+ Rating" disabled>1800+ Rating</option>
                                        <option value="2000+ Rating" disabled>2000+ Rating</option>
                                        <option value="2200+ Rating" disabled>2200+ Rating</option>
                                        <option value="2400+ Rating" disabled>2400+ Rating</option>
                                        <option value="" disabled>---Rated Arenas---</option>
                                        <option value="Arena World Cup" disabled>Arena World Cup</option>
                                        <option value="" disabled>---Nonspecific---</option>
                                        <option value="Social Group" disabled>Social Group</option>
                                        <option value="Casual Progression" disabled>Casual Progression</option>
                                    </select>
                                    <div class="form-text">What are you trying to accomplish every tier/season? This isn't required.</div>
                                </div>

                                <!-- Your Goal -->
                                <div class="form-group mt-3">
                                    <label for="difficulty">Raid Difficulty:</label>
                                    <select class="form-control" name="difficulty">
                                        <option value="Mythic">Mythic</option>
                                        <option value="Heroic">Heroic</option>
                                        <option value="Normal">Normal</option>
                                    </select>
                                    <div class="form-text">This is specific to raiding teams, and isn't required.</div>
                                </div>

                                <!-- Your Timeslots  -->
                                <div class="form-group mt-3">
                                    <label for="timeslot">Your Required Days and Times:</label>
                                    <input class="form-control" type="text" name="timeslot" required>
                                    <div class="form-text">Ex: 8:00 to 12:00 CST Tues, Weds, Thurs</div>
                                </div>

                                <!-- Freeform Team Details -->
                                <div class="form-group mt-3">
                                    <label for="details">Any details or team information:</label>
                                    <textarea class="form-control" name="details" rows="4" cols="80"></textarea>
                                    <div class="form-text">This can be a blurb about your team, what you do, etc.</div>
                                </div>

                                <!-- Submit -->
                                <div class="form-group mt-3 text-center">
                                    <input class="btn btn-primary" type="submit" name="submit" value="LETS DO IT!" dusk="CreateTeamForm">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
