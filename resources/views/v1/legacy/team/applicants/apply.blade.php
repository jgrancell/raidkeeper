@extends('layouts.main')

@section('content')
    <div class="row mx-2">
        <div class="col-md-6 col-sm-12 subsection">
            <!-- Discord Card -->
            <div class="row mx-1 mt-2 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-left">{{ $team->name }} Overview</h3>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{ $team->getAttr("details") }}</li>
                            <li class="list-group-item"><strong>Time:</strong> {{ $team->getAttr('timeslot') }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @if($team->isApplicant($user->id))
            <div class="col-md-6 col-sm-12 subsection">
                    <!-- Apply Card -->
                <div class="row mx-2 mt-2 mb-4">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="text-center text-success">Application Pending</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @elseif(! $team->isMember($user->id))
        <div class="col-md-6 col-sm-12 subsection">
                <!-- Apply Card -->
            <div class="row mx-2 mt-2 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Apply To Join</h3>
                        </div>
                        <div class="card-body">
                            @if($user->battlenet()->count() == 1)
                                @if($team->isApplicant($user->id))
                                    <h3 class="text-center">Your application is pending.</h3>
                                @elseif($team->isMember($user->id))
                                @else
                                    <form action="/teams/{{$team->id}}/apply" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label for="Character">Character:</label>
                                            <select class="form-control" name="character_id" required>
                                                @foreach($user->characters() as $character)
                                                    @if(strtolower($team->getAttr('faction')) == strtolower($character->getAttr('faction')))
                                                        <option class="cls-{{$character->shortClassName()}}" value="{{$character->id}}">
                                                            {{$character->getAttr('name')}} - {{$character->getAttr('realm')}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <div class="form-text">This is the main character you want considered for the team.</div>
                                        </div>
                                        <div class="form-group">
                                            <label for="role">Role:</label>
                                            <select class="form-control" name="role" required>
                                                <option value="tank">Tank</option>
                                                <option value="healer">Healer</option>
                                                <option value="mdps">DPS</option>
                                            </select>
                                            <div class="form-text">This is the role you want to play on the team.</div>
                                        </div>
                                        <div class="form-group mt-2 d-flex justify-content-center">
                                            <input type="submit" class="btn btn-primary" name="Submit" value="Submit Application">
                                        </div>
                                    </form>
                                @endif
                            @else
                                <div class="d-grid d-flex justify-content-center">
                                    <h3 class="text-center fac-horde">You have not connected a Battle.net account to your Raidkeeper account.</h3>
                                </div>
                                <div class="d-grid d-flex justify-content-center">
                                    <a class="mr-a ml-a btn btn-primary btn-lg" href="/user/profile">Visit Your Profile</a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    @if($user->battlenet()->count() == 1)
    <div class="row mx-2">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Eligible Characters</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm text-center">
                        <thead>
                            <tr>
                                <th style="width: 25%">Character</th>
                                <th style="width: 15%">Spec+Covenant</th>
                                <th style="width: 10%">iLvl</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($user->battlenet()->count() == 1)
                                @foreach($user->characters() as $character)
                                    @if(strtolower($character->getAttr('faction')) == strtolower($team->getAttr('faction')))
                                        <tr>
                                            <td class="cls-{{$character->shortClassName()}}">
                                                {{ $character->getAttr('name') }}
                                            </td>
                                            <td>
                                                <img style="height: 32px;" src="/img/icons/specs/{{ $character->iconSpec() }}.png"></img>
                                                <img style="height: 32px; width: 32px;" src="/img/icons/covenants/{{ $character->iconCovenant() }}.png"></img>
                                            </td>
                                            <td>
                                                @if($team->getAttr('minimum_ilvl') && $character->getAttr('ilvl_equipped') < $team->getAttr('minimum_ilvl'))
                                                    <span class="cls-deathknight">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                @else
                                                    @if($character->getAttr('ilvl_equipped') >= config('app.ilvls.mythic'))
                                                        <span class="text-warning">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                    @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.heroic'))
                                                        <span class="cls-demonhunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                    @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.normal'))
                                                        <span class="cls-shaman">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                    @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.raid-finder'))
                                                        <span class="cls-hunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                    @else
                                                        <span style="color: #999;">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
