<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GreatVault extends Model
{
    use HasFactory;

    protected $fillable = [
        'character_id',
        'period',
        'raid_1',
        'raid_2',
        'raid_3',
        'key_1',
        'key_2',
        'key_3',
        'pvp_1',
        'pvp_2',
        'pvp_3',
    ];
}
