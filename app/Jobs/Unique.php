<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldBeUnique;

class Unique extends Base implements ShouldBeUnique
{
    /**
     * The unique model ID of the job
     */
    public function uniqueId()
    {
        return $this->model->id;
    }
}
