<?php

namespace App\Http\Controllers\Teams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\TeamAttribute;
use App\Models\TeamOfficer;
use Auth;

class IndexController extends Controller
{
    public function index(): \Illuminate\View\View
    {
        return view(config('rk.version.ui') . '.teams.index');
    }

    public function form(): \Illuminate\View\View
    {
        return view(config('rk.version.ui') . '.teams.form');
    }

    public function create(Request $request): \Illuminate\Http\RedirectResponse
    {
        $count = Team::where([
            'region' => $request->input('region'),
            'name'   => $request->input('name'),
        ])->count();

        if ($count == 0) {
            $team = new Team();
            $team->region     = $request->input('region');
            $team->name       = $request->input('name');
            $team->short_name = $request->input('short_name');
            $team->user_id    = Auth::user()->id;
            $team->save();

            $team->attributes()->save(new TeamAttribute(['key' => 'realm','value' => $request->input('realm')]));

            $officership = new TeamOfficer();
            $officership->team_id = $team->id;
            $officership->user_id = Auth::user()->id;
            $officership->save();
            return redirect($team->url());
        } else {
            return redirect()->back()->with(['alert' => 'danger', 'message' => 'The team name+region you have selected is already in use..']);
        }
        return redirect('/teams/create');
    }
}
