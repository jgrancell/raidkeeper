@extends('layouts.team-officer')

@section('subcontent')
    <div class="row mx-2">
        <!-- Team Members -->
        <div class="col-12 subsection">
            <div class="row mx-0 mt-4 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Team Members</h3>
                        </div>
                        <div class="card-body">
                            @if (count($team->members()) == 0)
                                <h4 class="text-success text-center">
                                    There are currently no team members.
                                </h4>
                            @else
                                <div class="table-responsive">
                                    <table class="table table-sm-text-center">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Username</th>
                                                <th>Rank</th>
                                                <th>Characters</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($team->members() as $user)
                                                <tr class="text-center">
                                                    <td>{{$user->username}}#{{$user->discriminator}}</td>
                                                    <td>
                                                        {{$team->isOfficer($user->id) ? 'Officer' : 'Member'}}
                                                    </td>
                                                    <td>
                                                        @foreach($user->characters(strtolower($team->getAttr('faction'))) as $character)
                                                            <img style="width: 28px; height: 28px;" src="/img/icons/classes/{{strtolower($character->getAttr('class'))}}.png" alt="{{$character->getAttr('class')}}">
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        <a href="/teams/{{$team->id}}/members/{{$user->id}}/remove"><i class="text-danger fas fa-times"></i></a>
                                                        <a href="/teams/{{$team->id}}/recruitment/{{$user->id}}/promote"><i class="text-warning fas fa-plus"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
