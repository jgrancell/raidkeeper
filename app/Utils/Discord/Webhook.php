<?php

namespace App\Utils\Discord;

class Webhook
{
    public static function send($payload, string $endpoint, bool $isEmbed = false)
    {

        //$payload = [
        //    'username'   => 'Botty Boatick',
        //    'avatar_url' => 'https://i.imgur.com/qYWo04F.png',
        //    'embeds'     => [],
        //    'content'    => '',
        //];
//
        //if ($isEmbed) {
        //    $payload['embeds'][] = $message;
        //} else {
        //    $payload['content'] = $message;
        //}
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL            => $endpoint,
            CURLOPT_HTTPHEADER    => ['Content-Type: application/json'],
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POSTFIELDS     => json_encode($payload),
        ));
        $response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        echo "Webhook completed with status: ".$status.PHP_EOL;
        echo $response.PHP_EOL;
        curl_close($ch);
    }
}
