<header class="header">
    <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow">
        <a class="sidebar-toggler me-4 me-lg-5 lead" style="color: white;" href="#">
            <img src="/v2/images/vendor/hamburger.svg" alt="">
        </a>

        <a class="navbar-brand fw-bold text-uppercase text-base" href="/">
            <img src="/branding/logos/{{$theme}}.png" style="height: 65px;" alt="">
        </a>
        @if(Request::routeIs('team.*') && Request::route('team') !== null)
            <a class="navbar-brand fw-bold text-uppercase text-base" style="display" href="{{Request::route('team')->url()}}">
                <span class="d-inline-block" style="display: inline-block; padding-top: 40px; font-size: 1.1rem;">{{Request::route('team')->name}}</span>
            </a>
        @endif
        <ul class="ms-auto d-flex align-items-center list-unstyled mb-0">
            @if(\Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="/me">
                        <span class="sidebar-link-title" style="font-size: 1.1rem">Characters</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/me/settings">
                        <span class="sidebar-link-title" style="font-size: 1.1rem">Settings</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">
                        <span class="sidebar-link-title" style="font-size: 1.1rem">Logout</span>
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="/login">
                        <span class="sidebar-link-title" style="font-size: 1.1rem">Login</span>
                    </a>
                </li>       
            @endif
        </ul>
    </nav>
</header>