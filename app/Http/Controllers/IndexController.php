<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Team;

class IndexController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view(config('rk.version.ui') . '.index');
    }
}
