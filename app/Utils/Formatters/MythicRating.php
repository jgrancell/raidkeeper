<?php declare(strict_types=1);

namespace App\Utils\Formatters;

class MythicRating
{

    public static function parse(mixed $rating): string
    {
        $rating = intval($rating);

        if ($rating >= config('rk.mplus_thresholds.legendary.minimum')) {
            $cssClass = 'wow-legendary';
            $ilvl     = config('rk.mplus_thresholds.legendary.ilvl');
        } elseif ($rating >= config('rk.mplus_thresholds.epic.minimum')) {
            $cssClass = 'wow-epic';
            $ilvl     = config('rk.mplus_thresholds.epic.ilvl');
        } elseif ($rating >= config('rk.mplus_thresholds.rare.minimum')) {
            $cssClass = 'wow-rare';
            $ilvl     = config('rk.mplus_thresholds.rare.ilvl');
        } elseif ($rating >= config('rk.mplus_thresholds.uncommon.minimum')) {
            $cssClass = 'wow-uncommon';
            $ilvl     = config('rk.mplus_thresholds.uncommon.ilvl');
        } else {
            $cssClass = 'wow-common';
            $ilvl     = config('rk.mplus_thresholds.common.ilvl');
        }

        return '<span data-bs-toggle="tooltip" data-bs-placement="top" title="Valor Item Level Max: '.$ilvl.'" class="h4 rk-'.$cssClass.'">'.$rating . '</span>';
    }
}