@extends('v3._templates.main')

@section('content')
<div class="row">
  <img src="https://via.placeholder.com/500x100.png" alt="">
</div>
<div class="row rk-content">
  <h3>Raidkeeper (RK) is a World of Warcraft team management tool.</h3>
  <p>
    RK serves as your one-stop shop for building, growing, and
    managing a Raid, PvP, or Mythic+ team. RK is currently undergoing active development, so feel free to
    stop by our <a href="/link/discord">Discord</a> to provide suggestions and feedback.
  </p>
</div>
<div class="row rk-content">
  <p>Raidkeeper has a wide number of features that you can use to keep track of your Team's progress in game, including:</p>
  <br>

  <h4 class="rk-content-title">Roster Management</h4>
  <p>
    Easily add and remove characters to your team, or update their current role (tank/healer/dps) or bench them.
  </p>

  <h4 class="rk-content-title">Weekly Great Vault Tracking</h4>
  <p>
    Keep an eye on whatever requirements you've set for your team's weekly Great Vault? Are people missing keystones? RK will let you see exactly what they've achieved in their Great Vault so far this week.
  </p>
  <h4 class="rk-content-title">Character Drilldowns</h4>
  <p>See what equipment your team members are wearing, whether they've got all of their Tier Gear, how many sockets/enchants they're missing, etc.</p>
</div>
<div class="row rk-content align-items-center">
  <div class="col-12 align-items-center text-center">
    <a class="btn btn-large btn-primary" href="/teams/create">Create A Team Now!</a>
  </div>
  
</div>
@endsection