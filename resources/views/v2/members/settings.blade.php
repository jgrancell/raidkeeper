@extends('v2.core.main')

@section('headline')
Settings and Preferences
@endsection

@section('content')
<!-- Keystones Section -->
<section class="mb-4 mb-lg-5">
    <div class="row">
        <div class="col-md-6">
            <div class="card mb-3 h-100">
                <div class="card-header">
                    <h3 class="card-heading" style="font-size: 1.6rem;">Account Settings</h3>
                </div>
                <div class="card-body px-2 py-2">
                    Placeholder
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card mb-3 h-100">
                <div class="card-header">
                    <h3 class="card-heading" style="font-size: 1.6rem;">Display Preferences</h3>
                </div>
                <div class="card-body px-4 py-4">
                    <form action="/me/settings" method="POST" id="theme-color">
                        @csrf
                        <div class="mb-1">
                            <label class="form-label" for="theme-color">Raidkeeper Theme Color</label>
                            <select class="form-select form-select" name="theme-color">
                                @foreach(['blue', 'green', 'red', 'gold', 'pink', 'purple', 'silver', 'orange'] as $color)
                                    <option value="{{$color}}"@if($user->getSetting('theme-color') == $color) {{'selected'}} @endif>{{ucwords($color)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection