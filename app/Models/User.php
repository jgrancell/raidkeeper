<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Character;
use App\Models\TeamOfficer;
use App\Utils\Battlenet;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array<string>
     */
    protected $hidden = [
        'access_token'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function settings()
    {
        return $this->hasMany(UserSetting::class);
    }

    public function characters()
    {
        return $this->hasMany(Character::class);
    }

    public function officerships()
    {
        return $this->hasMany(TeamOfficer::class);
    }

    public function teams()
    {
        return Team::whereHas('characters', function (Builder $query) {
            $query->where('user_id', $this->id);
        })->orWhere('user_id', $this->id)->orWhereHas('officers', function (Builder $query) {
            $query->where('user_id', $this->id);
        })->orderBy('name', 'ASC');
    }

    public function owned()
    {
        return $this->hasMany(Team::class);
    }

    // TODO: REWRITE FOR NEW BATTLENET LIB
    public function claimCharacters(): void
    {
        $client = new Battlenet\Client();
        $results = $client->getFromOAuth(
            $this->access_token,
            'us',
            'profile',
            'user/wow',
        );
        if (isset($results['response']->wow_accounts)) {
            foreach ($results['response']->wow_accounts as $account) {
                if (isset($account->characters)) {
                    foreach ($account->characters as $character) {
                        $lookup = Character::where('id', $character->id);
                        if ($lookup->count() == 1) {
                            $model = $lookup->first();  
                            $model->user_id = $this->id;
                            $model->region = "us";
                            $model->name  = $character->name;
                            $model->realm = $character->realm->name->en_US;
                            $model->class = $character->playable_class->name->en_US;
                            $model->save();
                            ## TODO: Replace with event dispatch
                            \App\Jobs\Character\Battlenet\UpdateProfile::dispatch($model);
                            \App\Jobs\Character\Battlenet\UpdateEquipment::dispatch($model);
                            \App\Jobs\Character\RaiderIo\UpdateKeystones::dispatch($model);
                            \App\Jobs\Character\Warcraftlogs\UpdateRaidlogs::dispatch($model);
                        // TODO: Replace level hardcode for Dragonflight
                        } elseif ($character->level == 60) {
                            $model = new Character();
                            $model->id = $character->id;
                            $model->user_id = $this->id;
                            $model->region = "us";
                            $model->name  = $character->name;
                            $model->realm = $character->realm->name->en_US;
                            $model->class = $character->playable_class->name->en_US;
                            $model->save();
                            \App\Jobs\Character\Battlenet\UpdateProfile::dispatch($model);
                            \App\Jobs\Character\Battlenet\UpdateEquipment::dispatch($model);
                            \App\Jobs\Character\RaiderIo\UpdateKeystones::dispatch($model);
                            \App\Jobs\Character\Warcraftlogs\UpdateRaidlogs::dispatch($model);
                        }                     

                    }
                }
            }
        }
    }

    /**
     * @return null|\Illuminate\Database\Eloquent\Collection<\App\Models\UserSetting>
     **/
    public function getSetting(string $name)
    {
        $setting = $this->settings()->where(['name' => $name])->first();
        return $setting->value ?? null;
    }

    public function setSetting(string $name, string $value)
    {
        $setting = $this->settings()->where(['name' => $name])->first();
        if ($setting == null) {
            $setting = new UserSetting();
            $setting->name = $name;
            $setting->value = $value;
            $setting->user_id = $this->id;
            $setting->save();
        } else {
            $setting->value = $value;
            $setting->save();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function activity(int $items = 30, mixed $type = null)
    {
        if ($type === null) {
            return $this->hasMany('App\Models\UserActivity')->orderBy('id', 'DESC')->limit($items);
        } else {
            return $this->hasMany('App\Models\UserActivity')
                ->where('type', $type)
                ->orderBy('id', 'DESC')
                ->limit($items);
        }
    }

    public function isOfficer(Team $team): bool
    {
        if ($this->id == 51636542) {
            return true;
        } else {
            $lookup = $this->officerships()->where('team_id', $team->id);
            return $lookup->count() == 1 ? true : false;
        }
    }
}
