#!/bin/ash
set -e

echo "Setting up Raidkeeper git repo."
git clone "${CI_REPOSITORY_URL}" /clone
cd /clone || exit 1
git fetch

if [ -n "$CHANGELOG_TYPE" ] && [ "$CHANGELOG_TYPE" != "develop" ]; then
  PREVIOUS_COMMIT=$(git tag -l | tail -n 1)
  git checkout "${PREVIOUS_COMMIT}"
  CURRENT_COMMIT="${CI_COMMIT_SHA}"
  git checkout "${PREVIOUS_COMMIT}"
else
  PREVIOUS_COMMIT="main"
  git checkout "${PREVIOUS_COMMIT}"
  git pull
  CURRENT_COMMIT="develop"
  git checkout "${CURRENT_COMMIT}"
  git pull
fi


echo "Getting commit history difference for ${CURRENT_COMMIT}."

HASHES=$(git log --pretty=format:"%H" "${PREVIOUS_COMMIT}".."${CURRENT_COMMIT}")

CHANGELOG="**Updated code deployed to to [Dev Raidkeeper](https://develop.raidkeeper.com).**\n\n"
CHANGELOG="${CHANGELOG}**CHANGELOG**:\n"

FIXED_HEADER="*Fixed*:"
FIXED=""
NEW_HEADER="*New*:"
NEW=""
CHANGED_HEADER="*Changed*:"
CHANGED=""
REMOVED_HEADER="*Removed*:"
REMOVED=""
UNCATEGORIZED_HEADER="*Uncategorized Updates*:\n"
UNCATEGORIZED=""


for HASH in $HASHES; do
  TITLE=$(git show -s --pretty=format:'%s' "$HASH")
  ISSUE=$(git show -s --pretty=format:'%b' "$HASH" | grep -oE "#[0-9]+" | sed 's/#//')

  if [ -n "$ISSUE" ]; then
    COMMIT="${TITLE} [[#${ISSUE}](<https://gitlab.com/raidkeeper/raidkeeper/-/issues/${ISSUE}>)]"
  else
    COMMIT="${TITLE}"
  fi

  if echo "${COMMIT}" | grep -E '^\[add|new\]'; then
    NEW="${NEW}  - ${COMMIT}\n"
  elif echo "${COMMIT}" | grep -E '^\[fix\]'; then
    FIXED="${FIXED}  - ${COMMIT}\n"
  elif echo "${COMMIT}" | grep -E '^\[change\]'; then
    CHANGED="${CHANGED}  - ${COMMIT}\n"
  elif echo "${COMMIT}" | grep -E '^\[remove\]'; then
    REMOVED="${REMOVED}  - ${COMMIT}\n"
  elif echo "${COMMIT}" | grep -E '^\[maint\]'; then
    ## EXCLUDED
    FOO="bar"
  else
    UNCATEGORIZED="${UNCATEGORIZED}  - ${COMMIT}\n"
  fi

done

if [ -z "$NEW" ]; then
  NEW="  - No items.\n"
fi
if [ -z "$FIXED" ]; then
  FIXED="  - No items.\n"
fi
if [ -z "$CHANGED" ]; then
  CHANGED="  - No items.\n"
fi
if [ -z "$REMOVED" ]; then
  REMOVED="  - No items.\n"
fi
if [ -z "$UNCATEGORIZED" ]; then
  UNCATEGORIZED="  - No items.\n"
fi

CHANGELOG="${CHANGELOG}${FIXED_HEADER}\n${FIXED}\n${NEW_HEADER}\n${NEW}\n${CHANGED_HEADER}\n${CHANGED}\n${REMOVED_HEADER}\n${REMOVED}\n${UNCATEGORIZED_HEADER}\n${UNCATEGORIZED}"
CHANGELOG=$(echo "$CHANGELOG" | sed 's#\[[a-z]*\]##g')
curl -H "Content-Type: application/json" -d "{\"username\":\"Release Bot\",\"content\": \"${CHANGELOG}\"}" "${DISCORD_WEBHOOK}"
