@extends('layouts.main')

@section('navigation')

@endsection

@section('content')
    <div class="row mx-2">
        <div class="col-md-6 col-sm-12 subsection">
            <!-- Discord Card -->
            <div class="row mx-1 mt-2 mb-4">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-left">{{ $team->name }} Overview</h3>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{ $team->getAttr("details") }}</li>
                            <li class="list-group-item"><strong>Time:</strong> {{ $team->getAttr('timeslot') }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 subsection">
            @if($isOfficer)
                <!-- The logged in user is an Officer -->
                <!-- Discord Card -->
                <div class="row mx-2 mt-2 mb-4">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="text-center">Pending Applications</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-sm-text-center">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Character</th>
                                                <th style="width: 10%">Role</th>
                                                <th style="width: 10%">iLvl</th>
                                                <th style="width: 10%"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($team->applications()->get() as $app)
                                                <tr class="text-center">
                                                    <td class="cls-{{$app->character()->shortClassName()}}">
                                                        <img style="width: 20px; height: 20px;" src="/img/icons/covenants/{{$app->character()->iconCovenant()}}.png"></img>
                                                        {{$app->character()->getAttr('name')}}
                                                    </td>
                                                    <td><img style="width: 30px; height: 30px;" src="/img/icons/{{ $app->role }}.svg"></img></td>
                                                    <td>{{$app->character()->getAttr('ilvl_equipped')}}</td>
                                                    <td>
                                                        <a href="/teams/{{$team->id}}/apps/{{$app->id}}/accept"><i class="text-success fas fa-plus"></i></a>
                                                        <a href="/teams/{{$team->id}}/apps/{{$app->id}}/reject"><i class="text-danger fas fa-times"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Battle.Net Card -->
                <div class="row mx-2 mt-2 mb-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="text-center">Raid Breakdown</h3>
                            </div>
                            <div class="card-body">
                                <div class="row text-center">
                                    <div class="col-4">
                                        <h2>
                                            @if ($characters['breakdown']['tank'] >= 2)
                                                <span class="text-success">{{$characters['breakdown']['tank']}}</span>
                                            @else
                                                <span class="text-danger">{{$characters['breakdown']['tank']}}</span>
                                            @endif
                                        </h2>
                                        <h4>Tanks</h4>
                                    </div>
                                    <div class="col-4">
                                        <h2>
                                            @if ($characters['breakdown']['healer'] >= (($characters['breakdown']['tank'] + $characters['breakdown']['healer'] + $characters['breakdown']['mdps']) / 5))
                                                <span class="text-success">{{$characters['breakdown']['healer']}}</span>
                                            @else
                                                <span class="text-danger">{{$characters['breakdown']['healer']}}</span>
                                            @endif
                                        </h2>
                                        <h4>Healers</h4>
                                    </div>
                                    <div class="col-4">
                                        <h2>{{$characters['breakdown']['mdps']+$characters['breakdown']['rdps']}}</h2>
                                        <h4>DPS</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="row mx-2">

    </div>

    @if($isOfficer)
        <div class="row mx-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">Available Alts</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm text-center">
                            <thead>
                                <tr>
                                    <th style="width: 25%">Character</th>
                                    <th style="width: 10%">Account</th>
                                    <th style="width: 15%">Spec+Covenant</th>
                                    <th style="width: 10%">iLvl</th>
                                    <th style="width: 10%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($characters['available'] as $character)
                                    <tr>
                                        <td class="cls-{{$character->shortClassName()}}">{{ $character->getAttr('name') }}</td>
                                        @if($character->isGhost())
                                            <td>Manually Added Ghost</td>
                                        @else
                                            <td>{{$character->getUsername()}}</td>
                                        @endif
                                        <td>
                                            <img style="height: 32px;" src="/img/icons/specs/{{ $character->iconSpec() }}.png"></img>
                                            <img style="height: 32px; width: 32px;" src="/img/icons/covenants/{{ $character->iconCovenant() }}.png"></img>
                                        </td>
                                        <td>
                                            @if($team->getAttr('minimum_ilvl') && $character->getAttr('ilvl_equipped') < $team->getAttr('minimum_ilvl'))
                                                <span class="cls-deathknight">{{ $character->getAttr('ilvl_equipped') }}</span>
                                            @else
                                                @if($character->getAttr('ilvl_equipped') >= config('app.ilvls.mythic'))
                                                    <span class="text-warning">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.heroic'))
                                                    <span class="cls-demonhunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.normal'))
                                                    <span class="cls-shaman">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                @elseif($character->getAttr('ilvl_equipped') >= config('app.ilvls.raid-finder'))
                                                    <span class="cls-hunter">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                @else
                                                    <span style="color: #999;">{{ $character->getAttr('ilvl_equipped') }}</span>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <form class="" action="/teams/{{$team->id}}/roster" method="POST">
                                                @csrf
                                                <input type="hidden" name="character_id" value="{{$character->id}}">
                                                <input class="btn btn-empty" type="submit" name="submit" value="Add To Roster">
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    @endif
@endsection
