<?php

namespace App\Policies;

use App\Models\Character;
use App\Models\User;
use App\Models\Team;
use Illuminate\Auth\Access\HandlesAuthorization;

class CharacterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Character  $character

     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Character $character)
    {
        // Permitted if user owns character
        if ($user->id === $character->user_id) {
            return true;
        }

        // Permitted if user is Officer of team character is on
        foreach($character->teams()->get() as $team) {
            if ($user->isOfficer($team)) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Character $character)
    {
        return $user->id === $character->user_id;
    }
}
