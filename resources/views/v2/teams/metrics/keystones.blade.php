@extends('v2.core.main')

@section('headline')
Metrics > Keystones
@endsection

@section('content')
<!-- Equipment Section -->
<section class="mb-4 mb-lg-5">
    <div class="card card-table mb-4">
        <div class="card-header">
            <div class="h3 text-gray-100 text-uppercase">Team Keystone Metrics - Showing >= +{{$level}}</div>
        </div>
        <table class="table table-hover mb-0" id="datatable2">
            <thead>
                <tr class="text-center">
                    <th style="text-align: left;">Character</th>
                    <th>Role</th>
                    <th>Benched Kids</th>
                    <th>Current Week</th>
                    <th>Last Week</th>
                    <th>3 Weeks Ago</th>
                    <th>4 Weeks Ago</th>
                    <th>5 Weeks Ago</th>
                </tr>
            </thead>
            <tbody class="text-center">
                @foreach($characters as $character)
                    <tr>
                        <td style="text-align: left;">{{$character->name}}</td>
                        <td>
                            {{\App\Utils\Formatters\Role::parseForHumans($character->pivot->role)}}
                        </td>
                        <td>
                            @if($character->pivot->benched === 1)
                                <span class="btn btn-danger">Benched</span>
                            @else
                                ---
                            @endif
                        </td>
                        @for($period = $currentPeriod; $period != ($currentPeriod - 5); $period--)
                            <td>{{$character->keystones->where('period', $period)->count()}}</td>
                        @endfor
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
@endsection