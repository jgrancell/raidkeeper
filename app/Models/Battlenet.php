<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Battlenet extends Model
{
    use HasFactory;

    /**
     * @var array<string>
     **/
    protected $fillable = [
        'id',
        'user_id',
        'battletag',
        'token',
        'expiration'
    ];

    /**
     * @var array<string>
     **/
    protected $hidden = [
        'token'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function characters(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Character::class);
    }

    public function saveCharacter(string $name, string $realmSlug, bool $ghost = false): \App\Models\Character
    {
        $profile = $this->get(
            'us',
            'profile',
            'wow/character/'.strtolower($realmSlug).'/'.urlencode(strtolower($name))
        );
        $result = null;
        if ($profile['error'] == null) {
            $char = Character::where('character_id', $profile['response']->id)->first();
            if ($char === null) {
                // This character is new to us
                $char = Character::create([
                    'character_id'     => $profile['response']->id,
                    'battlenet_id' => $this->id,
                ]);
            } else {
                if ($ghost) {
                    dd('Character already registered on RaidKeeper.');
                } else {
                    // The character already exists, so we'll update the userid
                    //    in case there has been an account transfer.
                    $char->battlenet_id = $this->id;
                }
            }
            $char->save();
            $result = $char;

            $attrMap = [
                'name'             => $profile['response']->name,
                'realm'            => strtolower($profile['response']->realm->name->en_US),
                'class'            => strtolower($profile['response']->character_class->name->en_US),
                'race'             => strtolower($profile['response']->race->name->en_US),
                'faction'          => strtolower($profile['response']->faction->name->en_US),
                'level'            => $profile['response']->level,
                'covenant'         => strtolower($profile['response']->covenant_progress->chosen_covenant->name->en_US),
                'renown'           => $profile['response']->covenant_progress->renown_level,
                'ilvl_avg'         => $profile['response']->average_item_level,
                'ilvl_equipped'    => $profile['response']->equipped_item_level,
                'spec'             => strtolower($profile['response']->active_spec->name->en_US),
            ];
            foreach ($attrMap as $key => $value) {
                $attr = CharacterAttribute::updateOrCreate(
                    [
                        'character_id' => $char->id,
                        'key'    => $key,
                    ],
                    [
                        'value'  => $value,
                    ]
                );
            }

            if ($ghost) {
                CharacterAttribute::updateOrCreate(
                    [
                        'character_id' => $char->id,
                        'key'    => 'isGhost',
                    ],
                    [
                        'value'  => 'true',
                    ]
                );
            }
        } else {
            dd($profile);
        }
        return $result;
    }

    public function fetchCharacters(): bool
    {
        $lookup = $this->get('us', 'profile', 'user/wow');

        if ($lookup['error'] === null) {
            // We get every associated WOW account, so parsing them each.
            foreach ($lookup['response']->wow_accounts as $account) {
                // Breaking apart each account into characters
                foreach ($account->characters as $character) {
                    // Making sure any character we add is at the current max level
                    if ($character->level == config('app.max_level')) {
                        // Working through the pertinent information we want to save for the character
                        $profile = $this->saveCharacter($character->name, $character->realm->slug);
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array<mixed>
     **/
    public function get(string $region, string $namespace, string $endpoint): array
    {
        $headers = [
            'Authorization: Bearer ' . $this->token,
            'Battlenet-Namespace: ' . $namespace . '-' . $region,
            'User-Agent: RaidKeeper <josh@joshgrancell.com>',
        ];

        $ch = curl_init();
        curl_setopt_array($ch, array (
            CURLOPT_URL => 'https://'.$region.'.api.blizzard.com/'.$namespace.'/' . $endpoint,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status == 200) {
            return [
                // @phpstan-ignore-next-line
                'response' => json_decode($response),
                'error'    => null,
            ];
        } else {
            return [
                'response' => $response,
                'error'    => $status,
            ];
        }
    }
}
