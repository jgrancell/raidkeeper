<ul class="list-unstyled">
    <li class="sidebar-list-item">
        <a class="sidebar-link" href="#" data-bs-target="#teamsDropdown" role="button" aria-expanded="false" data-bs-toggle="collapse"> 
            <svg class="svg-icon svg-icon-md me-3">
                <use xlink:href="/v2/images/vendor/orion-svg-sprite.svg#more-1"> </use>
            </svg>
            <span class="sidebar-link-title">Your Teams</span>
        </a>
        <ul class="sidebar-menu list-unstyled collapse " id="teamsDropdown">
            @foreach(\Auth::user()->teams()->get() as $ut)
                <li class="sidebar-list-item">
                    <a class="sidebar-link text-muted" href="{{$ut->url()}}">
                        {{$ut->name}}{{$ut->user_id == \Auth::user()->id ? " ⭐" : "" }}
                    </a>
                </li>
            @endforeach
            <li class="sidebar-list-item">
                <a class="sidebar-link text-muted" href="/teams/create">
                    + Create A Team
                </a>
            </li>
        </ul>
    </li>
</ul>
