<?php

namespace App\Http\Controllers\Teams;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Character;
use App\Models\Team;
use Raidkeeper\Api\Battlenet\Client;
use Raidkeeper\Api\Battlenet\ApiResponse;

class RosterController extends Controller
{
    public function index(Team $team, Request $request, mixed $filter = null): \Illuminate\View\View
    {
        $cacheKey = $filter == 'bench' ? 'rk_team_'.$team->id.'_roster_bench' : 'rk_team_'.$team->id.'_roster_active';
        $roster = Cache::get($cacheKey);

        if ($roster === null) {
            $team->cacheRoster();
            $roster = Cache::get($cacheKey);
        }

        $characters  = $roster['characters'];
        $filter      = $roster['filter'];
        $showColumns = $roster['showColumns'];

        return view(config('rk.version.ui') . '.teams.roster', compact('team', 'characters', 'filter', 'showColumns'));
    }

    public function update(Team $team, Request $request)
    {
        switch ($request->input('action')):
            case 'create':
                $this->create($team, $request);
                break;
            case 'edit':
                $this->editMember($team, $request);
                break;
            default:
            return redirect()->back()->with(['alert' => 'danger', 'message' => 'Unable to process request. Action not found.']);
        endswitch;
        $team->cacheRoster();
        return redirect()->back();
    }

    public function remove(Team $team, Request $request)
    {
        $character = $team->characters()->where('character_id', $request->input('character_id'))->first();
        if ($character === null) {
            return redirect()->back()->with(['alert' => 'danger', 'message' => 'You do not have access to the selected character.']);
        }

        $character->pivot->delete();
        $team->cacheRoster();
        return redirect()->back();
    }


    public function editMember(Team $team, Request $request)
    {
        $character = $team->characters()->where('character_id', $request->input('character_id'))->first();
        if ($character === null) {
            return redirect()->back()->with(['alert' => 'danger', 'message' => 'You do not have access to the selected character.']);
        }

        // Updating their Role
        $character->pivot->role = $request->input('role');
        $character->pivot->save();

        // Updating their bench status
        $benched = $request->input('status') == 'bench';
        $character->pivot->benched = $benched;
        $character->pivot->save();

        $trial = $request->input('trial') == 'trial';
        $character->pivot->trial = $trial;
        $character->pivot->save();
        $character->save();
        return redirect()->back();
    }

    public function create(Team $team, Request $request)
    {
        $name      = $request->input('name');
        $realm     = $request->input('realm');
        $region    = $request->input('region');
        $status    = $request->input('status');
        $trial     = $request->input('trial');
        $role      = $request->input('role');

        $client = new Client($region, config('rk.battlenet.id'), config('rk.battlenet.secret'));
        $character = $client->loadCharacter($name, $realm);
        $data = $character->getProfile();
        if ($data instanceof ApiResponse) {
            $lookup = Character::where(['id' => $data->id, 'region' => $region]);
            if ($lookup->count() == 1) {
                $character = $lookup->first();
                $character->allow_fetch = true;
            } else {
                $character = new Character();
                $character->id = $data->id;
                $character->region = $region;
            }
            $character->name  = $data->name;
            $character->realm = $data->realm;
            $character->class = $data->character_class;
            $character->save();
            \Illuminate\Support\Facades\Bus::chain([
                new \App\Jobs\Character\Battlenet\UpdateProfile($character),
                new \App\Jobs\Character\Battlenet\UpdateEquipment($character),
                new \App\Jobs\Character\RaiderIo\UpdateKeystones($character),
                new \App\Jobs\Character\Warcraftlogs\UpdateRaidlogs($character),
                new \App\Jobs\Character\UpdateGreatVault($character),
                new \App\Jobs\Team\CacheRoster($team),
            ])->dispatch();

            $character->teams()->attach($team->id, ['role' => $role, 'benched' => $status, 'trial' => $trial]);
            return redirect($team->url().'/roster')->with(['message' => $name.' added to the roster!', 'alert' => 'success']);
        } else {
            return redirect()->back()->with([
                'message' => 'Unable to find '.$region.'/'.$realm.'/'.$name.' in the Battlenet API.',
                'alert' => 'danger'
            ]);
        }
        return redirect($team->url().'/roster');
    }

    public function refresh(Team $team, Character $character, Request $request)
    {
        if ($character !== null) {
            $character->allow_fetch = true;
            $character->save();
            \Illuminate\Support\Facades\Bus::chain([
                new \App\Jobs\Character\Battlenet\UpdateProfile($character),
                new \App\Jobs\Character\Battlenet\UpdateEquipment($character),
                new \App\Jobs\Character\RaiderIo\UpdateKeystones($character),
                new \App\Jobs\Character\Warcraftlogs\UpdateRaidlogs($character),
                new \App\Jobs\Character\UpdateGreatVault($character),
                new \App\Jobs\Team\CacheRoster($team),
            ])->dispatch();
        }
        return redirect($team->url().'/roster');
        
    }
}