<?php declare(strict_types=1);

namespace App\Utils\Formatters;

use Illuminate\Support\Facades\Cache;

class GreatVault
{
    public static function parse($lookup)
    {
        $output = '';

        foreach(['raid_1', 'raid_2', 'raid_3'] as $column) {
                $output .= static::raidHtml($lookup->$column ?? null);
        }

        $output .= '<span style="padding: .25rem;">|</span>';

        foreach(['key_1', 'key_2', 'key_3'] as $column) {
                $output .= static::keyHtml($lookup->$column ?? null);
        }

        //Cache::put($lookup->character_id.'_great_vault', $output, now()->addMinutes(10));
        return $output;
    }

    public static function raidHtml(mixed $item): string
    {
        switch ($item) :
            case 5:
                $class = 'rogue';
                $char  = 'M';
                break;
            case 4:
                $class = 'demonhunter';
                $char  = 'H';
                break;
            case 3:
                $class = 'shaman';
                $char  = 'N';
                break;
            case 2:
                $class = 'hunter';
                $char  = 'L';
                break;
            default:
                $class = 'deathknight';
                $char  = '-';
        endswitch;
        return static::html($class, $char);
    }

    public static function keyHtml(mixed $item): string
    {
        if ($item >= 20) {
            $class = "rogue";
        } elseif ($item >= 16) {
            $class = "demonhunter";
        } elseif ($item >= 11) {
            $class = "shaman";
            $item  = '0'.$item;
        } elseif ($item >= 7) {
            $class = "hunter";
            $item  = '0'.$item;
        } else { ($item >= 5)
            $class = 'deathknight';
            $item  = '0'.$item;
        }
        return static::html($class, $item);
    }

    public static function html(string $class, mixed $char): string
    {
        return '<span style="padding: .35rem" class="rk-cls-'.$class.'">'.$char.'</span>';
    }
}
