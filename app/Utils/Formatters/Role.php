<?php declare(strict_types=1);

namespace App\Utils\Formatters;

class Role
{
    public static function parse(int $id): string
    {
        switch ($id):
            case 1:
                return 'tank';
            case 2:
                return 'healer';
            case 3:
                return 'mdps';
            case 4:
                return 'rdps';
            default:
                return 'unknown';
        endswitch;
    }

    public static function parseForHumans(int $id): string
    {
        switch ($id):
            case 1:
                return 'Tank';
            case 2:
                return 'Healer';
            case 3:
                return 'MDPS';
            case 4:
                return 'RDPS';
            default:
                return '???';
        endswitch;
    }
}