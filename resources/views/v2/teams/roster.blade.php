@extends('v2.core.main')

@section('headline')
Roster
@endsection

@section('content')
<section class="mb-3 mb-lg-5">
    <div class="row">
        <!-- Roster Breakdown -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card h-100">
                <div class="card-body px-2 py-2" style="background-color: rgba(32,32,32,0.7);">
                    <div class="row">
                        @foreach(['tanks' => [1], 'healers' => [2], 'dps' => [3, 4]] as $name => $value)
                            <div class="col-4 text-center pt-2">
                                <h1 style="font-size: 3.5rem;" class="text-default">
                                    {{$characters->whereIn('pivot.role', $value)->count()}}
                                    <span style="font-size: 1rem">/ {{$characters->count()}}</span>
                                </h1>
                                <h5 class="text-gray-500 text-uppercase">{{ucwords($name)}}</h5>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer text-center py-2">
                    <h5 class="text-uppercase text-muted">{{$characters->count()}} Active Members</h5>
                </div>
            </div>
        </div>

        <!-- Raidwide Buffs -->
        <div class="col-xl-8 col-md-6 mb-4">
            <div class="card h-100">
                <div class="card-body px-2 py-2" style="background-color: rgba(32,32,32,0.7);">
                    <div class="row">
                        @php
                            $roleHealingCDs = [
                                'aura-mastery' => 'paladin',
                                'revival'      => 'monk',
                                'tranquility'  => 'druid',
                                'spirit-link'  => 'shaman',
                                'healing-tide' => 'shaman',
                                'salvation'    => 'priest',
                                'divine-hymn'  => 'priest',
                            ];
                            $classHealingCDs = [
                                'anti-magic-zone' => 'demon-hunter',
                                'rallying-cry'    => 'warrior',
                                'devotion-aura'   => 'paladin'
                            ];
                            $classBuffs = [
                                'fortitude'        => 'priest',
                                'battle-shout'     => 'warrior',
                                'arcane-intellect' => 'mage',
                                'chaos-brand'      => 'demon-hunter',
                                'mystic-touch'     => 'monk'
                            ];
                            $healingClasses = $characters->where('pivot.role', 2)->pluck('class');
                            $healingClasses = array_map('strtolower', $healingClasses->toArray());
                            $healingClasses = str_replace(' ', '-', $healingClasses);

                            $allClasses = $characters->pluck('class');
                            $allClasses = array_map('strtolower', $allClasses->toArray());
                            $allClasses = str_replace(' ', '-', $allClasses);
                        @endphp
                        <div class="col-6" style="border-right: 2px solid #444;">
                            <div class="d-flex flex-wrap justify-content-around align-self-center pt-4">
                                @foreach($roleHealingCDs as $cd => $class)
                                    <div style="text-align: center;">
                                        <img class="roster-icon rk-raid-cd" style="@if(! in_array($class, $healingClasses)){{"filter: grayscale(1.0)"}}@endif" alt="icon for {{$cd}}" src="/static/icons/spells/{{$cd}}.jpg"></img>
                                        <br />
                                        <p>{{ count(array_keys($healingClasses, $class)) }}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="d-flex flex-wrap justify-content-around align-self-center pt-4">
                                @foreach($classBuffs as $cd => $class)
                                <div style="text-align: center;">
                                    <img class="roster-icon rk-raid-cd" style="@if(! in_array($class, $allClasses)){{"filter: grayscale(1.0)"}}@endif" alt="icon for {{$cd}}" src="/static/icons/spells/{{$cd}}.jpg"></img>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center py-2">
                    <h5 class="text-uppercase text-muted">Raidwide Buffs and Cooldowns</h5>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Roster -->
<section class="mb-4 mb-lg-5">
    <div class="card card-table mb-4">
        <div class="card-header py-3">
            <div class="h3 text-gray-100 text-uppercase" style="float: left">
                {{$filter === null ? "Active" : ucwords($filter)}} Raid Roster
            </div>
            <div style="float: right">
                <a class="btn btn-secondary" type="button" href="{{$team->url()}}/roster{{ $filter === null || $filter === 'active' ? '/bench' : '/active' }}">Toggle Benched</a>
            </div>
            @can('update', $team)
            <div style="float: right; margin-right: 5px;">
                <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#newRaider">Add Raider</button>
            </div>
            @endcan
        </div>
        @include('v2.teams.roster.table', ['bench' => true, 'active' => true])
    </div>
</section>
<!-- End Keystones Section -->

<!-- Modal-->
<div class="modal fade text-start" style="margin-top: 20%;" id="newRaider" tabindex="-1" role="dialog" aria-labelledby="newRaiderLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <h3 class="h5 text-uppercase modal-title" id="newRaiderLabel">Add A New Raider</h3>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Character information will be automatically fetched in the background, and will be available within a few minutes.</p>
                <form action="{{$team->url()}}/roster" method="POST">
                    @csrf
                    <input type="hidden" name="action" value="create">
                    <div class="input-group mb-2">
                        <label class="input-group-text" for="region">Region:</label>
                        <select class="form-select" name="region" id="region" default-realm="{{$team->attributes()->where('key', 'realm')->first()->value ?? null}}" required>
                            <option value="" disabled>--Select A Region--</option>
                            <option value="us" @if($team->getAttribute('region') == 'us'){{'selected'}}@endif>US</option>
                            <option value="eu" @if($team->getAttribute('region') == 'eu'){{'selected'}}@endif>EU</option>
                            <option value="apac" @if($team->getAttribute('region') == 'apac'){{'selected'}}@endif>APAC</option>
                        </select>
                    </div>
                    <div class="input-group mb-2">
                        <label class="input-group-text" for="realm">Server:</label>
                        <select class="form-select" name="realm" id="realm" required>
                            <option disabled>--Select Realm First--</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <label class="input-group-text" for="name">Character:</label>
                        <input class="form-control" type="text" name="name" id="name" placeholder="Character Name" required>
                    </div>
                    <div class="input-group mb-3">
                        <label class="input-group-text" for="status">Status:</label>
                        <select class="form-select" name="status" id="status">
                            <option value="0" selected>Active</option>
                            <option value="1">Benched</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <label class="input-group-text" for="trial">Trial:</label>
                        <select class="form-select" name="trial" id="trial">
                            <option value="0" selected>No</option>
                            <option value="1">Yes</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <label class="input-group-text" for="role">Role:</label>
                        <select class="form-select" name="role" id="role">
                            <option value="1">Tank</option>
                            <option value="2">Healer</option>
                            <option value="3">Melee DPS</option>
                            <option value="4">Ranged DPS</option>
                            <option value="5">Flex</option>
                        </select>
                    </div>
                    <div class="input-group text-right align-items-right">
                        <input type="submit" class="btn btn-primary text-right" value="Add To Roster">
                    </div>
                    <div class="form-group"></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
