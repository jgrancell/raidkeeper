<?php

namespace App\Utils\Battlenet;

class Client
{
    public function getFromEndpoint(string $region, string $namespace, string $endpoint): array
    {
        $url     = 'https://'.$region.'.api.blizzard.com/'.$namespace.'/'.$endpoint;
        $token   = new Token($region);
        $headers = [
            'Authorization: Bearer ' . $token->getToken(),
            'Battlenet-Namespace: ' . $namespace . '-' . $region,
            'User-Agent: RaidKeeper <github.com/raidkeeper/raidkeeper>',
        ];
        $response = $this->get($url, $headers);
        return $response;
    }

    public function getFromUrl(string $region, string $url): array
    {
        $token = new Token($region);
        $headers = [
            'Authorization: Bearer ' . $token->getToken(),
            'User-Agent: RaidKeeper <github.com/jgrancell/raidkeeper>',
        ];
        $response = $this->get($url, $headers);
        return $response;
    }

    public function getFromOAuth(string $accessToken, string $region, string $namespace, string $endpoint)
    {
        $url     = 'https://'.$region.'.api.blizzard.com/'.$namespace.'/'.$endpoint;
        $headers = [
            'Authorization: Bearer ' . $accessToken,
            'Battlenet-Namespace: ' . $namespace . '-' . $region,
            'User-Agent: RaidKeeper <github.com/jgrancell/raidkeeper>',
        ];
        $response = $this->get($url, $headers);
        return $response;
    }

    public function get(string $url, array $headers)
    {

        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL            => $url,
                CURLOPT_HTTPHEADER     => $headers,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_SSL_VERIFYPEER => 0,
            ),
        );

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($status == 200) {
            return [
                // @phpstan-ignore-next-line
                'response' => json_decode($response),
                'error'    => null,
            ];
        }
        return [
            'response' => $response,
            'error'    => $status,
        ];
    }
}
