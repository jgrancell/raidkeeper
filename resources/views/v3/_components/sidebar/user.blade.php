<li class="nav-item">
  <a href="/foobar" class="nav-link rk-sidebar-header">User Management</a>
</li>

<li class="nav-item">
  <a href="/me" class="nav-link rk-sidebar-link"><i class="lni lni-users"></i> Characters</a>
</li>

<li class="nav-item">
  <a class="nav-link collapsed rk-sidebar-link" data-bs-toggle="collapse" data-bs-target="#myTeamsCollapse" aria-expanded="false">
    <i class="lni lni-arrow-down-circle"></i> Your Teams &#9660;
  </a>
  <div class="collapse" id="myTeamsCollapse">
    <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
      <li class="nav-item rk-subnav-item">
        <a href="/me/metrics" class="nav-link rk-sidebar-link pl-2"><i class="lni lni-arrow-right-circle"></i> Foobar Team</a>
      </li>
    </ul>
  </div>
</li>

<li class="nav-item">
  <a href="/me/metrics" class="nav-link rk-sidebar-link"><i class="lni lni-pie-chart"></i> Weekly Metrics</a>
</li>

<li class="nav-item">
  <a href="/me/settings" class="nav-link rk-sidebar-link"><i class="lni lni-cog"></i> Account Settings</a>
</li>

<li class="nav-item">
  <a href="/logout" class="nav-link rk-sidebar-link"><i class="lni lni-cross-circle"></i> Logout</a>
</li>