@extends('v2.core.main')

@section('headline')
Overview
@endsection

@section('content')
<section class="mb-3 mb-lg-5">
    <div class="row">
        @if(str_contains($callouts, 'total_members'))
            <!-- Active Raider Count -->
            <div class="col-xl-3 col-12 mb-4">
                <div class="card-widget h-100" style="display: block">
                    <div class="text-center">
                        <div class="text text-center">
                            <h1 class="mb-0">{{ $characters->where('pivot.benched', false)->count() }} / {{$characters->count()}}</h1>
                            <span class="text-gray-300">active / total raiders</span>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <!-- Average Item Level Cards -->
        @foreach($ilvl_callouts as $role => $id)
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card-widget h-100" style="display: block">
                    <div class="">
                        <div class="text text-center">
                            <h1 class="mb-0">
                                {{round($characters->where('pivot.role', $id)->where('pivot.benched', false)->pluck('attributes')->flatten()->pluck('value')->avg(), 2)}}
                            </h1>
                            <span class="text-gray-300">average {{$role}} ilvl</span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        @if(str_contains($callouts, 'team_ilvl'))
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card-widget h-100" style="display: block">
                    <div class="">
                        <div class="text text-center">
                            <h1 class="mb-0">
                                {{round($characters->where('pivot.benched', false)->pluck('attributes')->flatten()->pluck('value')->avg(), 2)}}
                            </h1>
                            <span class="text-gray-300">average team ilvl</span>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(str_contains($callouts, 'weekly_keys'))
            <!-- weekly keys -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card-widget h-100" style="display: block">
                    <div>
                        <div class="text text-center">
                            <h1 class="mb-0">{{$characters->sum('keystones_count')}}</h1>
                            <span class="text-gray-300">completed keys this week</span>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</section>

<!-- Keystones Section -->
<section class="mb-4 mb-lg-5">
    <h2 class="section-heading section-heading-ms mb-4 mb-lg-5">Team Keystone Overview</h2>
    <div class="row">
        <div class="col-lg-7 mb-4 mb-lg-0">
            <div class="card h-100">
                <div class="card-header">
                    <h4 class="card-heading">Recent Completed Keys</h4>
                </div>
                <div class="card-body px-2 py-2">
                    <!-- Keystone Line -->
                    @foreach($team->keystonesUnique(Illuminate\Support\Facades\Cache::get('raiderio_period') ?? null) as $key)
                        <div class="col-12 keystone-feed">
                            <a class="message card px-5 py-3 mb-4 bg-hover-gradient-primary text-decoration-none text-reset" href="{{$key->raiderio_url}}" target="blank">
                                <div class="row">
                                    <div class="col-xl-3 d-flex align-items-center flex-column flex-xl-row text-center text-md-left">
                                        <strong class="h5 mb-0">{{(new \Carbon\Carbon($key->completed_at))->diffForHumans(null, null, true)}}</strong>
                                        <img class="avatar avatar-md p-1 mx-3 my-2 my-xl-0" src="/static/icons/dungeons/{{str_replace("'", "", str_replace(' ', '', strtolower($key->dungeon)))}}.jpg" alt="dungeon icon for {{$key->dungeon}}" style="max-width: 3rem">
                                        <h4 class="mb-0 d-xl-none d-xxl-inline">+{{ $key->level }}</h4>
                                    </div>
                                    <div class="col-xl-9 d-flex align-items-center flex-column flex-xl-row text-center text-md-left">
                                        <p class="mb-0 mt-3 mt-lg-0">
                                            <span class="d-none d-xl-inline d-xxl-none">+{{ $key->level }}</span>
                                            {{ $key->dungeon }}
                                            <span class="d-xl-none d-xxl-inline">
                                                @php
                                                    $runners             = $key->characters()->pluck('name')->flatten()->toArray();
                                                    $count               = count($runners) - 1;
                                                    $runners[$count] = 'and ' . $runners[$count];
                                                    $imploder            = $count >= 2 ? ', ' : ' ';
                                                    $runners             = ltrim(rtrim(implode($imploder, $runners), $imploder), 'and ');
                                                @endphp
                                                by <strong class="h6">{{$runners}}</strong>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        
        <div class="col-lg-5 mb-4 mb-lg-0">
            <!-- Affixes Breakout -->
            <div class="h-33 pb-4 pb-lg-2">
                <div class="card h-33">
                    <div class="card-body d-flex">
                        <div class="row w-100 align-items-center">
                            <div class="col-sm-5">
                                <h2 class="mb-0 align-items-center">
                                    <span>Current Weekly Affixes</span>
                                </h2>
                            </div>
                            <div class="col-sm-7 text-center">
                                <div class="d-flex">
                                    @foreach((\Illuminate\Support\Facades\Cache::get('raiderio_affixes') ?? []) as $affix)
                                    <div class="">
                                        <img src="https://wow.zamimg.com/images/wow/icons/large/{{$affix->icon}}.jpg" alt="{{$affix->name}} Icon">
                                        <span style="padding-top: 3px; font-size: .75rem">{{$affix->name}}</span>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Active Roster Keystone Breakout -->
            <div class="h-33 pb-4 pb-lg-2">
                <div class="card h-33">
                    <div class="card-body d-flex">
                        <div class="row w-100 align-items-center">
                            <div class="col-sm-5">
                                <h2 class="mb-0 align-items-center">
                                    <span>{{ $characters->where('pivot.benched', false)->count() == 0 ? 0 : floor(($characters->where('pivot.benched', false)->sum('target_keystone_count') / $characters->where('pivot.benched', false)->count()) * 100) }}%</span>
                                </h2>
                                <span class="text-muted text-uppercase small">of active characters</span>
                                <hr>
                                <small>with +{{$keyTarget}}s or better</small>
                            </div>
                            <div class="col-sm-7 text-center">
                                <h1 class="mb-0 align-items-center">
                                    <span class="rk-accent">{{ $characters->where('pivot.benched', false)->sum('target_keystone_count')}} active</span>
                                </h1>
                                <span class="text-uppercase small">with completed +{{$keyTarget}}s or better</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- +10 Keystone Breakout -->
            <div class="h-50 pt-lg-2">
                <div class="card h-100">
                    <div class="card-body d-flex">
                        <div class="row w-100 align-items-center">
                            <div class="col-sm-5 mb-4 mb-sm-0">
                                <h2 class="mb-0 d-flex align-items-center">
                                    <span>{{ $characters->where('pivot.benched', true)->count() == 0 ? 0 : floor(($characters->where('pivot.benched', true)->sum('target_keystone_count') / $characters->where('pivot.benched', true)->count()) * 100) }}%</span>
                                </h2>
                                <span class="text-muted text-uppercase small">of benched characters</span>
                                <hr>
                                <small>with +{{$keyTarget}}s or better</small>
                            </div>
                            <div class="col-sm-7 text-center">
                                <h1 class="mb-0 align-items-center">
                                    <span class="rk-accent">{{ $characters->where('pivot.benched', true)->sum('target_keystone_count')}} benched</span>
                                </h1>
                                <span class="text-uppercase small">with completed +{{$keyTarget}}s or better</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Keystones Section -->
@endsection