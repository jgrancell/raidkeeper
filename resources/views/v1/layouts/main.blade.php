<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>{{ __('core.title')}}</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.2/css/bootstrap.min.css" integrity="sha512-usVBAd66/NpVNfBge19gws2j6JZinnca12rAe2l+d+QkLU9fiG02O1X8Q6hepIpr/EYKZvKx/I9WsnujJuOmBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <!--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">-->
        <link rel="stylesheet" href="/css/app.css">
        <script src="/js/app.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.slim.js" integrity="sha512-HNbo1d4BaJjXh+/e6q4enTyezg5wiXvY3p/9Vzb20NIvkJghZxhzaXeffbdJuuZSxFhJP87ORPadwmU9aN3wSA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <!--<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>-->

    </head>
    <body class="d-flex flex-column min-vh-100">
        <nav class="navbar navbar-expand-md bg-dark header fixed-top">
            <div class="container">
                <a class="navbar-brand" href="/">{{__('core.title')}}</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarHeader">
                    <ul class="navbar-nav me-auto mb-2 mb-md-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="teamsDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Raid and M+ Teams
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="teamsDropdown">
                                @if(\Auth::user() != null)
                                    @foreach(\Auth::user()->teams() as $team)
                                        <li><a class="dropdown-item" href="/teams/{{$team->id}}">{{$team->name}}</a></li>
                                    @endforeach
                                @endif
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="/teams/new">Create New Team</a></li>
                            </ul>
                        </li>

                    </ul>
                    <div class="d-flex">
                        <ul class="navbar-nav me-auto mb-2 mb-md-0">
                            @if(\Auth::user())
                                <li class="nav-item dropdown">
                                    <span class="nav-link">
                                        Hello, {{\Auth::user()->username}}
                                    </span>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/logout">Logout</a>
                                </li>
                            @else
                                <li class="nav-item">
                                    <a class="nav-link" href="/login">Login</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <main class="container body-container flex-grow-1">
            @yield('content')
        </main><!-- /.container -->
        <footer class="footer bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-sm">
                        <p class="footer-copyright">
                            <a href="https://github.com/jgrancell/raidkeeper">Raidkeeper {{config('rk.version.application')}}</a>: {{ 'Page generated in ' . number_format((microtime(true) - LARAVEL_START), 4) . " seconds."}}
                            <br />Jobs: {{\Queue::size()}} Pending | N Complete | N Failed
                        </p>
                        
                    </div>
                    <div class="col-sm">
                        <p class="footer-copyright">
                            &copy; 2020-{{now()->year}} RaidKeeper. All rights reserved.
                        </p>
                    </div>
                    <div class="col-sm">

                        <p class="footer-copyright">
                            World of Warcraft, Warcraft, and Blizzard Entertainment are trademarks or registered trademarks of Blizzard Entertainment, Inc.
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.2/js/bootstrap.bundle.min.js" integrity="sha512-72WD92hLs7T5FAXn3vkNZflWG6pglUDDpm87TeQmfSg8KnrymL2G30R7as4FmTwhgu9H7eSzDCX3mjitSecKnw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" integrity="sha512-RXf+QSDCUQs5uwRKaDoXt55jygZZm2V++WUZduaU/Ui/9EGp3f/2KZVahFZBKGH0s774sd3HmrhUy+SgOFQLVQ==" crossorigin="anonymous"></script>
        <script src="/js/app.js" charset="utf-8"></script>
    </body>
</html>
