<?php declare(strict_types=1);

namespace App\Utils;

class Profiler
{
    protected int   $start; 
    protected mixed $checkpoints;

    public function __construct(string $name = null)
    {
        $this->checkpoints   = [];
        $this->checkpoints[] = [
            'name'       => $name ?? 'checkpoint-1',
            'since_last' => 0,
            'time'       => hrtime(true),
        ];
    }

    public function add(string $name = null)
    {
        $time     = hrtime(true);
        $previous = end($this->checkpoints);

        $this->checkpoints[] = [
            'name'       => $name ?? 'checkpoint-'.(count($this->checkpoints)+1),
            'since_last' => ($time - $previous['time']) / 100000000,
            'time'       => $time,
        ];
    }

    public function get()
    {
        return $this->checkpoints;
    }
}