<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\UpdateCharacterCore;
use App\Console\Commands\UpdateCharacterKeystones;
use App\Console\Commands\UpdateCharacterRaidlogs;
use App\Console\Commands\UpdateRosters;
use App\Console\Commands\GetWeeklyPeriod;
use App\Console\Commands\CacheTeamRosters;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array<mixed>
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (config('APP_ENV', 'production')) {
            $schedule->command(GetWeeklyPeriod::class)->everyThirtyMinutes();
            $schedule->command(UpdateCharacterCore::class)->hourly();
            $schedule->command(UpdateCharacterKeystones::class)->hourly();
            $schedule->command(UpdateCharacterRaidlogs::class)->everySixHours();
            $schedule->command(CacheTeamRosters::class)->everyFiveMinutes();
        } else {
            $schedule->command(GetWeeklyPeriod::class)->everyThirtyMinutes();
            $schedule->command(UpdateCharacterCore::class)->everyTwoHours();
            $schedule->command(UpdateCharacterKeystones::class)->everyFourHours();
            $schedule->command(UpdateCharacterRaidlogs::class)->daily();
            $schedule->command(CacheTeamRosters::class)->hourly();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
