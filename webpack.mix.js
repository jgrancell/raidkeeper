const mix = require('laravel-mix');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
// V1 UI
mix.sass('resources/v1/scss/app.scss', 'public/v1/css');

// V2 UI
mix.sass('resources/v2/scss/blue.scss', 'public/v2/css/blue.css', {implementation: require('node-sass')})
   .sass('resources/v2/scss/gold.scss', 'public/v2/css/gold.css', {implementation: require('node-sass')})
   .sass('resources/v2/scss/green.scss', 'public/v2/css/green.css', {implementation: require('node-sass')})
   .sass('resources/v2/scss/pink.scss', 'public/v2/css/pink.css', {implementation: require('node-sass')})
   .sass('resources/v2/scss/red.scss', 'public/v2/css/red.css', {implementation: require('node-sass')})
   .sass('resources/v2/scss/purple.scss', 'public/v2/css/purple.css', {implementation: require('node-sass')})
   .sass('resources/v2/scss/silver.scss', 'public/v2/css/silver.css', {implementation: require('node-sass')})
   .sass('resources/v2/scss/orange.scss', 'public/v2/css/orange.css', {implementation: require('node-sass')})
   .purgeCss({enabled: true, safelist: { deep:[/rk-/, /^show$/, /alert/, /btn/]}});

mix.js('resources/v2/js/app.js', 'public/v2/js/app.js');

// V3 UI
mix.sass('resources/v3/scss/app.scss', 'public/v3/css')
   .sass('resources/v3/scss/theme.scss', 'public/v3/css')
   .sass('resources/v3/scss/theme-red.scss', 'public/v3/css')
   .sass('resources/v3/scss/theme-blue.scss', 'public/v3/css');

   mix.js('resources/v3/js/app.js', 'public/v3/js');