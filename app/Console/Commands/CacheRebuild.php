<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;
use Illuminate\Bus\Batch;

class CacheRebuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rk:cache:rebuild';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuilds all of the pertinent cached values RK requires.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Bus::batch([
            [
                new \App\Jobs\Admin\UpdateWeeklyPeriod,
            ],
        ])->then(function (Batch $batch) {
            foreach(\App\Models\Team::all() as $team) {
                \App\Jobs\Team\CacheRoster::dispatch($team);
            };
        })->name('Rebuild Cache')->dispatch();
    }
}
