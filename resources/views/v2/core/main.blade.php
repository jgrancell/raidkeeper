<!DOCTYPE html>
<html lang="{{'en'}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RaidKeeper</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <meta http-equiv="content-security-policy" content="default-src 'self'; font-src https://fonts.gstatic.com; img-src 'self' data: https://wow.zamimg.com; object-src 'none'; script-src 'self' 'unsafe-inline' https://wow.zamimg.com https://cdn.jsdelivr.net; style-src 'unsafe-inline' 'self' https://fonts.googleapis.com https://wow.zamimg.com; connect-src https://www.wowhead.com https://nether.wowhead.com">
        <!-- Google fonts - Popppins for copy-->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">
        <!-- The Main Theme stylesheet (Contains also Bootstrap CSS)-->
        @php
            // TODO: Simplify this big time
            $theme = 'blue';
            if (\Auth::check()) {
                $settings = \Auth::user()->settings()->where('name', 'theme-color')->first();
                $theme = $settings == null ? 'blue' : $settings->value;
            }
        @endphp
        <link rel="stylesheet" href="/v2/css/{{$theme}}.css" id="theme-stylesheet">
        <!-- Favicon-->
        <link rel="shortcut icon" href="/branding/favicons/{{$theme}}.png">
    </head>
    <body>
        <!-- navbar-->
        @include('v2.core.header')
        @include('v2.core.content')
        <!-- JavaScript files-->
        <script src="/js/bootstrap.min.js" integrity="sha512-OvBgP9A2JBgiRad/mM36mkzXSXaJE9BEIENnVEmeZdITvwT09xnxLtT4twkCa8m/loMbPHsvPl0T8lRGVBwjlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest"></script>
        <!-- Main Theme JS File-->
        <script src="/js/vendor/theme.js"></script>
        
        <!-- Application Javascript-->
        <script src="/js/app.js"></script>
    </body>
</html>