<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCharacterKeystoneTableSetPrimaryKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('character_keystone', function (Blueprint $table) {
            $table->unique(['character_id', 'keystone_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('character_keystone', function (Blueprint $table) {
            $table->dropUnique(['character_id', 'keystone_id']);
        });
    }
}
