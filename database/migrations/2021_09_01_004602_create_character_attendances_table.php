<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacterAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_attendances', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('character_id');
            $table->bigInteger('team_attendance_id');
            $table->integer('team_id');
            $table->integer('status');
            $table->timestamps();

            $table->unique(['character_id', 'team_attendance_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_attendances');
    }
}
