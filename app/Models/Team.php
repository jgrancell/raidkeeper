<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Utils\RaiderIo\MythicPlus;
use Illuminate\Support\Facades\Cache;

class Team extends Model
{
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function attributes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TeamAttribute::class);
    }

    public function officers(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(User::class, 'team_officers', 'team_id', 'user_id');
    }

    public function characters(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Character::class, 'team_memberships', 'team_id', 'character_id')->withPivot(['role', 'benched', 'trial']);
    }

    public function keystones(): \Illuminate\Support\Collection
    {
        $characters = $this->characters()->with('keystones')->get();
        return $characters->pluck('keystones')->flatten()->sortByDesc('completed_at',);
    }

    public function keystonesUnique(int $period = null, int $limit = 5, string $order = 'DESC'): \Illuminate\Support\Collection
    {
        $keystones = $this->keystones();
        if ($period !== null) {
            $keystones = $keystones->where('period', $period);
        }
        
        $collection = $keystones->sortByDesc('completed_at');
        $return = $collection->unique(function ($item) {
            return $item->completed_at.$item->dungeon.$item->level;
        });

        return $return->take($limit);
    }

    public function cacheRoster(): void
    {
        $period = Cache::get('raiderio_period');

        // Column views
        $columns = [
            'great_vault'  => false,
            'mythic_score' => false,
            'tier_special' => false,
        ];
        $selectedColumns = $this->attributes()->where('key', 'default_roster_columns')->first()->value ?? "";
        foreach (array_keys($columns) as $column) {
            $showColumns[$column] = str_contains($selectedColumns, $column);
        }

        $pivotToCacheMap = [
            'active' => [
                'cacheKey'   => 'rk_team_'.$this->id.'_roster_active',
                'pivotValue' => false,
            ],
            'bench' => [
                'cacheKey'   => 'rk_team_'.$this->id.'_roster_bench',
                'pivotValue' => true,
            ],
        ];

        foreach($pivotToCacheMap as $filter => $map) {
            $fetch = $this->characters()->wherePivot('benched', $map['pivotValue']);
            $characters = $fetch->with([
                'keystones'  => function (\Illuminate\Database\Eloquent\Relations\BelongsToMany $query) use ($period) { $query->where('period', $period); },
                'great_vault' => function (\Illuminate\Database\Eloquent\Relations\HasMany $query) use ($period) { $query->where('period', $period); },
                'attributes' => function (\Illuminate\Database\Eloquent\Relations\HasMany $query) { $query->where('key', 'faction')->orWhere('key', 'equipped_ilvl')->orWhere('key', 'bliz_mythic_rating'); },
                'user',
            ])->orderBy('pivot_role')->get();

            $roster = [
                'characters'  => $characters,
                'showColumns' => $showColumns,
                'filter'      => $filter,
            ];

            Cache::set($map['cacheKey'], $roster);
        }
    }

    public function url(): string
    {
        return '/teams/'.$this->region.'/'.str_replace(' ', '+', $this->name);
    }

    public function setAttr(string $key, string $value): void
    {
        $lookup = TeamAttribute::where(['team_id' => $this->id, 'key' => $key]);
        if ($lookup->count() != 1) {
            $attribute = new TeamAttribute();
            $attribute->team_id = $this->id;
            $attribute->key          = $key;
        } else {
            $attribute = $lookup->first();
        }
        $attribute->value = $value;
        $attribute->save();
    }
}
